program integral

use precision
implicit none
real(dl)::adq,v,q,dum(16),dummy,temp,am,aq,q,y,aqdn,total,z

integer iq,a,c

am=0
open(1,file='checker.dat')

do c=1,100

adq=1._dl
dum(1)=0._dl
do iq=1,15
q=iq*adq
aq=am/q
v=1._dl/sqrt(1._dl+aq*aq)
aqdn=adq*q*q*q/(exp(q)+1._dl)
dum(iq+1)=aqdn*v*v*v*am*am/(q*q)
end do
call splint(dum,temp,16)
temp=temp+dum(16)*2._dl/14.5_dl

total=0

do a=1,200000
   q=0.0001*a-0.00005
   v=1._dl/sqrt(q*q+am*am)
   y=q**4*am*am/(exp(q)+1._dl)*v*v*v
   total=total+0.0001*y
!   write(1,'(2E15.5)')q,y
end do


write(1,'(4E15.5)')am,total,temp,(total-temp)
am=am+20
end do

close(1)

contains

        subroutine splint(y,z,n)
        use Precision
!  Splint integrates a cubic spline, providing the output value
!  z = integral from 1 to n of s(i)di, where s(i) is the spline fit
!  to y(i).
!
        implicit none
        integer, intent(in) :: n
        real(dl), intent(in) :: y(n)
        real(dl), intent(out) :: z

        integer :: n1
        real(dl) :: dy1, dyn
!
        n1=n-1
!  Cubic fit to dy/di at boundaries.
!       dy1=(-11._dl*y(1)+18._dl*y(2)-9._dl*y(3)+2._dl*y(4))/6._dl
        dy1=0._dl
        dyn=(11._dl*y(n)-18._dl*y(n1)+9._dl*y(n-2)-2._dl*y(n-3))/6._dl
!
        z=0.5d0*(y(1)+y(n))+(dy1-dyn)/12._dl
        z= z + sum(y(2:n1))
        end subroutine splint

end program integral
