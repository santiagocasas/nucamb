!Exponential potential
!
! Equations module allowing for fairly general quintessence models
!
! by Antony Lewis (antony@camb.info)
! This version Oct 2004.
! Notes at http://antonylewis.com/notes/CAMB.ps

!This module is not well tested, use at your own risk!
!Need to specify Vofphi function, and also initial_phi in Quint_init_background
!You may also need to change other things to get it to work with different types of quintessence model

!It works backwards, in that it assumes Omega_lambda is Omega_Q today, then does a binary search on the 
!initial conditions to find what is required to give that Omega_Q today after evolution.


!!!This is a dummy for the default constant parameterization
module LambdaGeneral
use precision
real(dl)  :: w_lam = -1, cs2_lam=1  !p/rho for the dark energy (assumed constant) 
!         real(dl) :: sigma =  1.2247

logical :: k_started_matter = .false., k_started_nu = .false.
logical :: k_plotted_matter = .false., k_plotted_nu = .false.
end module LambdaGeneral



module Quint
use precision
use ModelParams 
use LambdaGeneral
implicit none
integer, parameter :: NumPoints = 2000, NumPointsEx=NumPoints+2+5000
!We actually calulate NumPointEx-NumPoints points after a=1 to get good interpolation at a=1
real(dl) phi_a(NumPointsEx),phidot_a(NumPointsEx)
real(dl) ddphi_a(NumPointsEx),ddphidot_a(NumPointsEx)

real(dl) adot,tempphi,tempphidot !Antz

logical, parameter :: has_extra = .true.  !If true Om_Lambda is interpreted as quintessence, otherwise just Lambda
real(dl) ::  da,initial_phi2,omnuant
!        real(dl), parameter :: m = 5d-7
real(dl),parameter::cx=2.99792458e8_dl

logical converge
logical,parameter::doconvergence=.false.
real ftol, Bestfit_loglike
integer,parameter::num_params_used=2

contains

!################################################################################################


subroutine checkconverge
	use MassiveNu 
	implicit none
	integer::acount,bcount,ccount


	write(*,*)'Beginning convergence search....'
	open(17,file='converge.dat')
	CP%beta=-1._dl
	CP%m=0.0000002_dl
	CP%sigma=-0.5_dl

	do acount=1,50
	write(*,*)'beta: ',CP%beta
	CP%beta=CP%beta+0.05_dl
	do bcount=1,50
	CP%m=CP%m+0.000000016_dl
	do ccount=1,50
	converge=.true.
	!write(*,*)'converge true: ',converge
	converge=.false.
	!write(*,*)'converge false: ',converge
	call  Quint_init_background
	!write(*,*)'converge : ',converge
	if (converge) then
		write(17,'(3E15.5)')CP%beta,CP%m,CP%sigma
		write(*,*)'these values worked: ',CP%beta,CP%m,CP%sigma
	else 
		write(*,*)'thse values did not converge',CP%beta,CP%m,CP%sigma
	end if
	CP%sigma=CP%sigma+0.05_dl
	end do
	CP%sigma=0.2_dl
	end do
	CP%m=0.0000003_dl
	end do
	write(*,*)'test me'
	close(17)
	!stop 'finished convergence'


end subroutine checkconverge


subroutine resultant
	use MassiveNu 
	implicit none
	integer jcount
	real(dl) zant,aant,tant,phi,phidot,pnu,rhonu,temp,temp2,totant,ztemp,dum,dum2(15),temp1
	real(dl) rhonuant,pnuant,rhocdm,rhophi,rhorad,adotoaant,rhonudot1,rhonudot2,betax,phimin,adot,rhocomb,pcomb
	real(dl) grhophi,gpphi,totp
	real(dl),parameter::eightpig=1.677004778e-9_dl
	real(dl),parameter::cx=2.99792458e8_dl
	real(dl) vel,lfs,dtauda,wtilda,rhophidot,kg1,kg2,kg3,kg4,xant,phitoday,phidottoday,rhonutoday,weff

	open(14,file='densities.dat')
	open(15,file='omega.dat')
	open(12,file='mass.dat')
	open(16,file='phi.dat')
	open(18,file='eos.dat')
	open(19,file='rhophidot.dat')
	open(20,file='beta.dat')
	open(21,file='wcomb.dat')
	zant=0.001_dl
	call Quint_ValsAta(1._dl,phitoday,phidottoday)
	call Nu_background(avarmnu(phitoday,0),rhonutoday,temp1)
	write(*,*)'nu mass today: ',avarmnu(phitoday,0)*1.68e-4
	do jcount=1,5083
	aant=1./(1.+zant)
	call Quint_ValsAta(aant,phi,phidot)
	call Nu_background(avarmnu(phi,0)*aant,rhonu,pnu)

	if (.not.(CP%beta==0)) then
		if (CP%pot_type==1) then
			phimin=log(CP%sigma*CP%m/(CP%beta*(rhonu-3*pnu)*grhormass/aant**4))/CP%sigma
		else if (CP%pot_type ==2) then
			phimin=exp(log(2*CP%m**6/(CP%beta*(rhonu-3*pnu)*grhormass/aant**4))/3)
		else if (CP%pot_type ==3) then
			phimin=exp(log(4*CP%m**8/(CP%beta*(rhonu-3*pnu)*grhormass/aant**4))/5)
		else if (CP%pot_type ==4) then
			phimin=exp(log(CP%sigma*CP%m**(4+CP%sigma)/(CP%beta*(rhonu-3*pnu)*grhormass/aant**4))/5)
		else
			stop 'Invalid choice of potential type'
		end if
	end if
	temp=aant**2*(0.5*phidot**2+aant**2*Vofphi(phi,0))
	totant=(grhoc+grhob)*aant+grhog+grhornomass+grhok*aant*aant+temp+grhormass*rhonu
	totp=aant**2*(0.5*phidot**2-aant**2*Vofphi(phi,0))+grhog/3._dl+grhormass*pnu
	adot=sqrt(totant/3._dl)
	write(15,'(5E15.5)')zant+1,grhormass*rhonu/totant,grhoc*aant/totant,temp/totant,grhog/totant  !grhormass*rhonu/totant
	rhonuant=grhormass/(aant**4*eightpig)*rhonu
	!rhonuant=grhornomass/(aant**3*eightpig)    for massless neutrinos
	rhocdm=grhoc/(aant**3*eightpig)
	rhophi=(phidot**2/2+aant*aant*Vofphi(phi,0))/(aant**2*eightpig)
	rhorad=grhog/(aant**4*eightpig)
	temp1= -aant*aant*(Vofphi(phi,1)+avarmnu(phi,1)*(grhormass)*(rhonu-3*pnu)/(avarmnu(phi,0)*aant**4))
	!write(20,'(3E15.5)')zant+1,2*adot*phidot/aant,temp1
	betax=avarmnu(phi,1)/avarmnu(phi,0)
	adotoaant=adot/aant

	write(16,'(5E15.5)')zant+1,phi,Vofphi(phi,0),(rhonu-3._dl*pnu)*grhormass/aant**4*exp(CP%beta*phi),aant
	!write(16,'(5E15.5)')zant+1,Timeofz(zant),rhonu*grhormass/aant**4,-3._dl*adotoaant*(rhonu+pnu)*grhormass/aant**4,(rhonu-3*pnu)*grhormass*betax*phidot/aant**4

	write(20,'(4E15.5)')zant+1,rhonu,pnu,adotoaant

	!write(*,*)betax
	!read(*,*)

	rhophidot=phidot*(-3*adotoaant*phidot/aant**2-betaofphi(phi)*(rhonu-3*pnu)*grhormass/aant**4)
	!write(19,'(4E15.5)')aant,TimeOfz(zant),rhophi*eightpig,rhophidot

	call Nu_derivs(avarmnu(phi,0)*aant,adotoaant,betax,rhonu,pnu,phidot,rhonudot1,dum,dum2,dum2)
	!write(*,*)'original2: ',rhonudot1
	rhonudot2=(rhonu-3*pnu)*(adotoaant+betax*phidot/4.09512488e-5_dl)
	!write(*,*)'new2: ',rhonudot2
	!write(19,'(3E15.5)')zant+1,rhonudot1,rhonudot2
	write(14,'(5E15.5)')zant+1,rhonuant,rhocdm,rhophi,rhorad
	!write(14,'(3E15.5)')zant+1,rhorad+rhonuant,(grhob/(aant**3*eightpig))+rhocdm
	write(12,'(2E15.5)')zant+1,avarmnu(phi,0)*1.68e-4
	grhophi=phidot**2/2+aant**2*Vofphi(phi,0)
	gpphi=phidot**2-grhophi
	wtilda=gpphi/grhophi+betax*phidot*(rhonu-3*pnu)*grhormass/(temp*3._dl*adotoaant)

	xant=(1-avarmnu(phi,0)/avarmnu(phitoday,0))*rhonutoday*grhormass/(aant**3*(phidot**2/(2._dl*aant**2)+Vofphi(phi,0)))
	weff=gpphi/grhophi/(1._dl-xant)
	write(18,'(6E15.6)')zant+1, pnu/rhonu,gpphi/grhophi,wtilda,weff,totp/totant
	!pnuant=CP%tcmb*4.658360991e-23/aant
	pnuant=CP%tcmb*3.374015137/aant
	vel=cx/sqrt(1+(avarmnu(phi,0))**2/(pnuant**2))
	lfs=vel*dtauda(aant)/aant

	temp=aant**2*(0.5*phidot**2+aant**2*Vofphi(phi,0))
	rhocomb=grhormass*rhonu+temp
	temp=aant**2*(0.5*phidot**2-aant**2*Vofphi(phi,0))
	pcomb=pnu*grhormass+temp



	!write(21,'(5E15.5)')aant,TimeOfz(zant),aant**2*(0.5*phidot**2-aant**2*Vofphi(phi,0)),aant**2*(0.5*phidot**2+aant**2*Vofphi(phi,0)),adot/aant
	zant=zant*1.005_dl
	end do
	write(*,*)'z+1, rhonu, rhocdm, rhoquint written to densities.dat'
	write(*,*)'z+1, mass written to mass.dat'
	write(*,*)'z+1, omeganu, omegacdm, omegaphi written to omega.dat'
	write(*,*)'z+1, phi written to phi.dat'
	write(*,*)'z+1, w written to eos.dat'
	write(*,*)'z+1, rhonudot (original), rhonudot (fixed) writted to rhonudot.dat'
	write(*,*)'a,pnu,vel,dtauda written to lfs.dat'

	write(*,*)'nu mass at high redshift = ',avarmnu(phi,0)*1.68e-4
	aant=1._dl
	write(*,*)'CP%m: ',CP%m
	call Quint_ValsAta(aant,phi,phidot)
	call Nu_background(avarmnu(phi,0)*aant,rhonu,pnu)


	temp=aant**2*(0.5*phidot**2+aant**2*Vofphi(phi,0))
	totant=(grhoc+grhob)*aant+grhog+grhornomass+grhok*aant*aant+temp+grhormass*rhonu
	!write(*,*)'beta: ',CP%beta
	write(*,*)'Om_nu:  ',grhormass*rhonu/totant
	write(*,*)'Om_nu h^2:  ',grhormass*rhonu/totant*(CP%h0/100._dl)**2
	!write(*,*)'Om_c:   ',grhoc*aant/totant
	!write(*,*)'Om_phi: ',temp/totant
	!write(*,*)'Om_g:   ',grhog/totant 
	!write(*,*)'Om_b:   ',grhob*aant/totant

	!write(*,*)
	write(*,*)'adotoa: ',sqrt(totant/3._dl)
	write(*,*)'CP%h0: ',CP%h0,grhom
	write(*,*)'check: ',sqrt(totant/3._dl*cx**2/1000**2)
	close(14)
	close(19)
	close(15)
	close(12)
	close(16)
	close(18)
	close(20)
	close(21)
	!write(*,*)'params: ',CP
	write(*,*)'nu method: ',CP%MassiveNuMethod

	!stop 'finished resultant'
end subroutine resultant

subroutine setuservals
	implicit none
	!open(13,file='beta.dat')
	!read(13,*)
	!read(13,*)CP%beta,CP%sigma,CP%m,CP%pot_type,CP%initial_phi
	!write(*,*)CP%beta,CP%sigma,CP%m,CP%pot_type,CP%initial_phi
	!close(13)
	!read(*,*)

	!CP%pot_type=1
	!CP%initial_phi=0.1
	!CP%m=5d-7
	!CP%fractemp=1
end subroutine setuservals

subroutine setmphi(okant)
	use MassiveNu
	implicit none
	real(dl)::astart,atol,om,tempexp,totant,omnuant,rhotot
	real(dl)::m1,m2,deltam,temp,rhonuz,pnuz,tempz
	real(dl),parameter::const=7._dl/120*pi**4
	real(dl),parameter::zeta3=1.20205690_dl
	integer::jitter,itter
	logical,intent(out)::okant
	logical::masscheck
	okant=.false.
	astart=1d-12
	!See if initial conditions are giving correct CP%omegav now
	atol=1d-8
	m1=1d-11
	m2=1d10
	!write(*,*)'current value of beta: ',CP%beta

	do jitter=1,100 

	deltam=m2-m1
	CP%m =m1 + deltam/2 
	temp=2._dl
	amnu=1000._dl
	if (CP%omegan==0._dl) then
		amnu=0._dl
	end if
	do itter=1,40
	!write(*,*)itter
	om = Quint_GetOmegaFromInitial(astart,CP%initial_phi,0._dl,atol)
	!write(*,*)'now'

	call setvarmnu(tempphi,tempphidot,masscheck)
	!write(*,*)'called setvarmnu'
	if (masscheck.and.(abs(temp-om)<1d-4)) then 
		exit
	end if
	!                   if (.not.masscheck) write(*,*) 'mass not set - switch to backup method....'
	temp=om

	!                tempexp=avarmnu(tempphi,0)/amnu
	!              amnu=const/(1.5d0*zeta3)*grhom/grhor*CP%omegan/((CP%Num_Nu_massive+1.0d-18))/tempexp
	end do
	om = Quint_GetOmegaFromInitial(astart,CP%initial_phi,0._dl,atol)
	call Nu_background(avarmnu(tempphi,0),rhonuz,pnuz) 

	tempz=(0.5*tempphidot**2+Vofphi(tempphi,0))
	totant=((grhoc+grhob)+grhog+grhornomass+grhok+grhormass*rhonuz)+tempz !/(1-CP%omegav)


	omnuant=rhonuz*grhormass/totant
	rhotot=(CP%h0)**2*3d6/cx**2

	omnuant=rhonuz*grhormass/rhotot
	!             write(*,*)'phi initial: ',CP%initial_phi
	!               write(*,*)'om: ',om, ' omegav: ',CP%omegav
	!                write(*,*)'mphi: ',CP%m
	!write(*,*)'tempz: ',tempz
	!write(*,*)'totant: ',totant,rhonuz*grhormass
	!read(*,*)

	if ((abs(om-CP%omegav) < 1d-3).and.(om>0).and.(om<1.).and.(abs(omnuant-CP%omegan)<1d-4)) then                 
		!                CP%m= (m2+m1)/2
		okant=.true.
		!                write(*,*)'om: ',om, ' omegav: ',CP%omegav
		!                write(*,*)'check om: ',tempz/totant
		!                write(*,*)'mphi: ',CP%m
		!                write(*,*)'omnu: ',omnuant,' CPomeganu: ',CP%omegan
		!                write(*,*)'totant vs rhotot: ',totant, rhotot
		!                write(*,*)'nu mass = ',avarmnu(tempphi,0)*1.68e-4,tempphi
		!read(*,*)
		!stop                
		return
	end if

	if (om < CP%omegav) then
		m1=CP%m
	else
		m2=CP%m
	end if

	end do
	write(*,*)
	write(*,*) 'failed to converge on mphi'
	write(*,*)'om: ',om, ' omegav: ',CP%omegav
	write(*,*)'mphi: ',CP%m
	write(*,*)'omnu: ',omnuant,' CPomeganu: ',CP%omegan
	write(*,*)'£££££££££££££££££££££££££££££££££££££££££££££'
end subroutine setmphi


subroutine mansetmphi(OKant,mphiant)
	use massivenu
	implicit none

	integer::counter,jiter
	real(dl)::mphiant,astart,atol,om,fred
	logical temp
	logical,intent(out)::OKant
	OKant=.false.
	do counter=1,200
	write(*,*)'do start: ',CP%m
	write(*,*)'Enter mphi initial: '
	write(*,*)'1: ',CP%m
	read(*,*)fred
	write(*,*)'2: ',CP%m
	if (fred<0._dl) then
		write(*,*)'3: ',CP%m
		OKant=.true.
		write(*,*)'quitting CP%m: ',CP%m
		exit
	end if
	CP%m=fred
	!if (phiant>999._dl) then exit
	astart=1d-12
	!See if initial conditions are giving correct CP%omegav now
	atol=1d-8
	amnu=1d-7
	do jiter=1,40
	om = Quint_GetOmegaFromInitial(astart,CP%initial_phi,0._dl,atol)
	call setvarmnu(tempphi,tempphidot,temp)
	end do
	write(*,*)
	write(*,*)'######################################################################'
	write(*,*)'mPhi: ',fred,' initial phi: ',CP%initial_phi
	write(*,*)'CP%m: ',CP%m
	write(*,*)'omegav:  ',om,'   omegan: ',getomeganunow(tempphi)
	write(*,*)'CP omegav: ',CP%omegav,'  CP omegan:  ',CP%omegan   
	write(*,*)'amnu: ',amnu
	if ((abs(om-CP%omegav)<1d-3).and.temp.and.(om>0).and.(om<1.)) then  !antz
		OKant=.true.
		CP%m=fred
		write(*,*)'Converged......'

		exit
	end if
	write(*,*)'here: ',CP%m
	end do
	if (.not. OKant) stop 'Unable to converge after 200 guessses... Give up.....'
end subroutine mansetmphi


subroutine setvarmnu(phinow,phidotnow,okant)
	use MassiveNu
	implicit none
	real(dl) phinow,phidotnow,rhotot
	real(dl) tempexp,initial_m,initial_m2,deltam,rhonuz,pnuz,omnu1,omnu2,temp,totant
	real(dl),parameter::const=7._dl/120*pi**4
	real(dl),parameter::zeta3=1.20205690_dl
	integer xiter
	logical,intent(out)::okant


	if (CP%omegan==0) then  !if no massive neutrinos...
		amnu=0._dl
		okant=.true.
		return
	end if

	if (CP%coupling_type == 1) then
		tempexp=exp(CP%beta*phinow)
	else if (CP%coupling_type == 2) then
		tempexp = 1.d0 / (CP%phi_crit - phinow)
	else
		stop 'Invalid coupling type in setvarmnu'
	end if
	!tempexp=cosh(CP%beta*phinow)
	!tempexp=1._dl+cos(CP%beta*phinow)
	!tempexp=(1._dl+cos(CP%beta*phinow))**0.5_dl
	!tempexp=avarmnu(phinow,0)/amnu
	!tempexp=exp(CP%beta*phinow*phinow)
	amnu=const/(1.5d0*zeta3)*grhom/grhor*CP%omegan/((CP%Num_Nu_massive+1.0d-18))/tempexp

	call Nu_background(avarmnu(phinow,0),rhonuz,pnuz) 
	temp=(0.5*phidotnow**2+Vofphi(phinow,0))
	rhotot=(CP%h0)**2*3d6/cx**2

	totant=((grhoc+grhob)+grhog+grhornomass+grhok+grhormass*rhonuz)+temp !/(1-CP%omegav)
	omnuant=rhonuz*grhormass/rhotot
	if (abs(omnuant-CP%omegan) < 1d-5) then                 
		okant=.true.
	else
		okant=.false.
	end if
	!write(*,*)'omnu: ',omnuant,'  CPomegan: ',CP%omegan
	!write(*,*)'amnu: ',amnu
	!okant=.true.
	!
	return

	!write(*,*)'gone wrong'
	!read(*,*)
	initial_m=1d-5
	initial_m2=1d8
	okant=.false.
	do xiter=1,100
	deltam=initial_m2-initial_m
	amnu =initial_m + deltam/2 
	call Nu_background(avarmnu(phinow,0),rhonuz,pnuz) 
	temp=(0.5*phidotnow**2+Vofphi(phinow,0))
	totant=((grhoc+grhob)+grhog+grhornomass+grhok+grhormass*rhonuz)+temp !/(1-CP%omegav)
	omnuant=rhonuz*grhormass/totant
	if (abs(omnuant-CP%omegan) < 1d-5) then                 
		!                initial_m = (initial_m2+initial_m)/2

		!write(*,*)'mass ok - found omnuant: ',omnuant
		!write(*,*)'amnu: ',amnu
		!write(*,*)'user set: ',CP%omegan
		!write(*,*)'diff: ',omnuant-CP%omegan
		!read(*,*)
		okant=.true.
		exit             
	end if
	if (omnuant < CP%omegan) then
		omnu1=omnuant
		initial_m=amnu
	else
		omnu2=omnuant
		initial_m2=amnu
	end if 

	end do !iterations
	if (.not.okant) then 
		!             stop 'mass not set....'
		!             write(*,*)'mass not set'
		!             read(*,*)
	end if

end subroutine setvarmnu

!subroutine setmphi
!need to define pot_type=0 still under construction...>>!!~~~~~~~~#############################
!use MassiveNu
!implicit none
!real(dl)::tempbeta,m,atol,astart
!integer::temppot_type

!tempbeta=CP%beta
!temppot_type=CP%beta
!CP%beta=0._dl
!CP%pot_type=0
!amnu=const/(1.5d0*zeta3)*grhom/grhor*CP%omegan/((CP%Num_Nu_massive+1.0d-18))
!m=0.000000113
!atol=1d-9
!astart=1d-8
!initial_m=1d-5
!initial_m2=1000000
!okant=.false.
!om1=Quint_GetOmegaFromInitial(astart,initial_phi,0.dl,atol)

!CP%m=m
!CP%beta=tempbeta
!CP%pot_type=temppot_type
!end subroutine setmphi

subroutine manfindphi(phi,okant)
	use MassiveNu
	implicit none
	real(dl) astart,atol,om,rhonuz,pnuz
	real(dl) initial_phi,initial_phi2,deltaphi,temp,temp2,totant
	real(dl),parameter::const=7._dl/120*pi**4
	real(dl),parameter::zeta3=1.20205690_dl
	integer xiter,jiter,signant
	logical,intent(out)::okant
	real(dl),intent(out)::phi
	logical masscheck

	astart=1d-12
	temp2=0
	atol=1d-8
	okant=.false.
	do xiter=1,200
	write(*,*)'enter phi: '
	read(*,*)phi
	temp=10._dl
	!amnu=1000
	do jiter=1,40
	om = Quint_GetOmegaFromInitial(astart,phi,0._dl,atol)
	call setvarmnu(tempphi,tempphidot,masscheck)

	if (masscheck) then 
		!                      om = Quint_GetOmegaFromInitial(astart,phi,0._dl,atol)
		write(*,*)'set mnu'
		exit
	end if
	!                   if (.not.masscheck) write(*,*) 'mass not set - switch to backup method....'
	temp=om
	end do
	!                if (.not.(abs(om-CP%omegav) < 1d-6)) then
	!                   stop 'i didnt jitter'
	!                end if
	om = Quint_GetOmegaFromInitial(astart,phi,0._dl,atol)
	call Nu_background(avarmnu(tempphi,0),rhonuz,pnuz) 
	temp=(0.5*tempphidot**2+Vofphi(tempphi,0))
	totant=((grhoc+grhob)+grhog+grhornomass+grhok+grhormass*rhonuz)+temp !/(1-CP%omegav)
	omnuant=rhonuz*grhormass/totant
	write(*,*)'amnu: ',amnu
	write(*,*)'omegav:  ',om,'  omegan: ',omnuant
	write(*,*)'CP omegav: ',CP%omegav,'  CP omegan:  ',CP%omegan
	write(*,*)'================================================='
	!            read(*,*)
	if ((abs(om-CP%omegav) < 1d-3).and.masscheck.and.(om>0).and.(om<1)) then                 
		okant=.true.
		!                   write(*,*)'phi0: ',phi,'   phi now: ',tempphi
		!             write(*,*)'omegav:  ',om,'  omegan: ',omnuant
		!             write(*,*)'CP omegav: ',CP%omegav,'  CP omegan:  ',CP%omegan
		!            write(*,*)'================================================='
		!            write(*,*)'sigma: ',CP%sigma
		exit
		!                   stop'im happy'     
	else
		temp=om
	end if
	end do !iterations

end subroutine manfindphi

subroutine findphi(phi,okant)
	use MassiveNu
	implicit none
	real(dl) astart,atol,om,rhonuz,pnuz
	real(dl) initial_phi,initial_phi2,deltaphi,temp,temp2,totant
	real(dl),parameter::const=7._dl/120*pi**4
	real(dl),parameter::zeta3=1.20205690_dl
	integer xiter,jiter,signant
	logical,intent(out)::okant
	real(dl),intent(out)::phi
	logical masscheck

	initial_phi=0.00001
	initial_phi2=1000
	!           phi=CP%initial_phi
	phi=(initial_phi+initial_phi2)/2 
	astart=1d-12
	temp2=0
	atol=1d-8
	okant=.false.
	do xiter=1,200
	temp=10._dl
	amnu=1000
	do jiter=1,40
	om = Quint_GetOmegaFromInitial(astart,phi,0._dl,atol)
	call setvarmnu(tempphi,tempphidot,masscheck)
	if (masscheck.and.(abs(temp-om)<1d-6)) then 
		!                      om = Quint_GetOmegaFromInitial(astart,phi,0._dl,atol)
		exit
	end if
	!                   if (.not.masscheck) write(*,*) 'mass not set - switch to backup method....'
	temp=om
	end do
	if (.not.(abs(temp-om))<1d-6) then
		stop 'i didnt jitter'
	end if
	if (CP%pot_type==1) then
		if (CP%sigma>0) then
			if (om > CP%omegav) then
				initial_phi=phi
			else
				initial_phi2=phi
			end if
		else
			if (om < CP%omegav) then
				initial_phi=phi
			else
				initial_phi2=phi
			end if
		end if
	else if (CP%pot_type>1) then
		if (om > CP%omegav) then
			initial_phi=phi
		else
			initial_phi2=phi
		end if
	else 
		stop 'findphi not configured for this potential....'
	end if

	call Nu_background(avarmnu(tempphi,0),rhonuz,pnuz) 
	temp=(0.5*tempphidot**2+Vofphi(tempphi,0))
	totant=((grhoc+grhob)+grhog+grhornomass+grhok+grhormass*rhonuz)+temp !/(1-CP%omegav)
	omnuant=rhonuz*grhormass/totant
	write(*,*)'phi: ',phi
	write(*,*)'omegav:  ',om,'  omegan: ',omnuant
	write(*,*)'CP omegav: ',CP%omegav,'  CP omegan:  ',CP%omegan
	write(*,*)'================================================='
	read(*,*)
	if ((abs(om-CP%omegav) < 1d-4).and.masscheck.and.(om>0).and.(om<1)) then                 
		!                   phi = (initial_phi+initial_phi2)/2
		okant=.true.
		!                   write(*,*)'phi0: ',phi,'   phi now: ',tempphi
		write(*,*)'omegav:  ',om,'  omegan: ',omnuant
		write(*,*)'CP omegav: ',CP%omegav,'  CP omegan:  ',CP%omegan
		write(*,*)'================================================='
		!            write(*,*)'sigma: ',CP%sigma
		exit     
	else
		if (abs(initial_phi-initial_phi2)<0.0000001) then
			initial_phi=phi-1
			initial_phi2=phi+1
		end if
		temp=om
		deltaphi=initial_phi2-initial_phi
		phi =initial_phi + deltaphi/2 
	end if
	end do !iterations


	if ((.not.okant).or.(.not.masscheck)) then 
		write(*,*)'sigma: ',CP%sigma
		write(*,*)'phi not found...'
		write(*,*)'best guess so far:'
		write(*,*)'phi: ',phi
		write(*,*)'omegav:  ',om,'  omegan: ',omnuant
		write(*,*)'CP omegav: ',CP%omegav,'  CP omegan:  ',CP%omegan
		write(*,*)'================================================='
		read(*,*)
	end if

end subroutine findphi


function getomeganunow(phi)
	implicit none
	real(dl) phi,massnu,getomeganunow
	real(dl),parameter::const=7._dl/120*pi**4
	real(dl),parameter::zeta3=1.20205690_dl
	massnu=avarmnu(phi,0)
	getomeganunow=massnu*1.5d0*zeta3/const*grhor/grhom*CP%Num_Nu_massive

end function getomeganunow




function avarmnu(phi,deriv)

	implicit none
	real(dl) phi,psi
	real(dl) avarmnu
	integer deriv

	!psi=phi/4.09512488e-5_dl
	psi=phi
	!exp coupling  !NOTE CHANGE fn setvarmnu to set with this coupling
	if (CP%coupling_type == 1) then
		if (deriv==0) then
			avarmnu=amnu*exp(CP%beta*psi)
		else if (deriv==1) then
			avarmnu=amnu*CP%beta*exp(CP%beta*psi)
		else if (deriv==2) then
			avarmnu=amnu*CP%beta*CP%beta*exp(CP%beta*psi)
		end if
	else if (CP%coupling_type == 2) then
		if (deriv == 0) then
			avarmnu = amnu / (CP%phi_crit - psi)
		else if (deriv == 1) then
			avarmnu = amnu / (CP%phi_crit - psi)**2
		else if (deriv == 2) then
			avarmnu = - 2.d0 * amnu / (CP%phi_crit - psi)**3
		end if
	else
		stop 'Invalid coupling type in avarmnu'
	end if

	!FNW 
	!if (deriv==0) then
	!   avarmnu=amnu/psi
	!else if (deriv==1) then
	!   avarmnu=amnu*-1._dl/(psi**2)
	!else if (deriv==2) then
	!   avarmnu=amnu*2._dl/(psi**3)
	!end if

	!cosh coupling   !NOTE CHANGE fn setvarmnu to set with this coupling
	!if (deriv==0) then
	!avarmnu=amnu*cosh(CP%beta*psi)
	!else if (deriv==1) then
	!avarmnu=amnu*CP%beta*sinh(CP%beta*psi)
	!else if (deriv==2) then
	!avarmnu=amnu*CP%beta*CP%beta*cosh(CP%beta*psi)
	!end if

	!cos coupling   !NOTE CHANGE fn setvarmnu to set with this coupling
	!if (deriv==0) then
	!avarmnu=amnu*(1._dl+cos(CP%beta*psi))
	!else if (deriv==1) then
	!avarmnu=-amnu*CP%beta*sin(CP%beta*psi)
	!else if (deriv==2) then
	!avarmnu=-amnu*CP%beta*CP%beta*cos(CP%beta*psi)
	!end if

	!root cos coupling   !NOTE CHANGE fn setvarmnu to set with this coupling
	!if (deriv==0) then
	!avarmnu=amnu*(1._dl+cos(CP%beta*psi))**0.5_dl
	!else if (deriv==1) then
	!avarmnu=-amnu*CP%beta*0.5_dl*sin(CP%beta*psi)/((1._dl+cos(CP%beta*psi))**0.5_dl)
	!else if (deriv==2) then
	!!avarmnu=-amnu*CP%beta*CP%beta*(1+cos(CP%beta*psi))**0.5_dl/4._dl
	!end if

	!if (deriv==0) then
	!avarmnu=amnu*exp(betaglobal/psi)
	!else if (deriv==1) then
	!avarmnu=-1._dl*amnu*betaglobal*exp(betaglobal/psi)/(psi**2)
	!else if (deriv==2) then
	!avarmnu=amnu*betaglobal*exp(betaglobal/psi)*(2._dl/(psi**3)+betaglobal/(psi**4))
	!end if

	!if (deriv==0) then   !NOTE CHANGE fn setvarmnu to set with this coupling
	!avarmnu=amnu*exp(CP%beta*psi*psi)
	!else if (deriv==1) then
	!avarmnu=2._dl*amnu*CP%beta*psi*exp(CP%beta*psi*psi)
	!else if (deriv==2) then
	!avarmnu=2._dl*amnu*CP%beta*exp(CP%beta*psi*psi)*(1+2._dl*CP%beta*psi)
	!end if

end function avarmnu

!Antz************************************************************************************

function betaofphi(phi)
	implicit none
	real(dl) phi, betaofphi

	if (CP%coupling_type == 1) then
		betaofphi = CP%beta
	else if (CP%coupling_type == 2) then
		betaofphi = -1.d0 / (phi - CP%phi_crit)
	else
		stop 'Invalid coupling type in betaofphi'
	end if

end function betaofphi


function Vofphi(phi,deriv)
	!Returns (8*Pi*G)^(1-deriv/2)*d^{deriv}V(psi)/d^{deriv}psi evaluated at psi
	!The input variable phi is sqrt(8*Pi*G)*psi

	implicit none
	real(dl) phi,Vofphi,ma,dmadphi,ddmaddphi
	integer deriv
	real(dl) mu
	!Example linear potential
	!     sigma= -sqrt(3./2.)         
	CP%pot_type=1
	if (CP%pot_type==1) then
		if (deriv==0) then
			Vofphi= CP%m*exp(-CP%sigma*phi)
		else if (deriv ==1) then
			Vofphi=-CP%m*CP%sigma*exp(-CP%sigma*phi)
		else if (deriv ==2) then
			Vofphi=CP%m*CP%sigma**2*exp(-CP%sigma*phi)
		else 
			stop 'Invalid deriv in Vofphi'
		end if
	else if (CP%pot_type==2) then
		if (deriv==0) then
			Vofphi= CP%m**6/phi**2
		else if (deriv ==1) then
			Vofphi=-2*CP%m**6/phi**3
		else if (deriv ==2) then
			Vofphi=6*CP%m**6/phi**4
		else 
			stop 'Invalid deriv in Vofphi'
		end if
	else if (CP%pot_type==3) then
		if (deriv==0) then
			Vofphi= CP%m**8/phi**4
		else if (deriv ==1) then
			Vofphi=-4*CP%m**8/phi**5
		else if (deriv ==2) then
			Vofphi=20*CP%m**8/phi**6
		else 
			stop 'Invalid deriv in Vofphi'
		end if
	else if (CP%pot_type==4) then
		if (deriv==0) then
			Vofphi=CP%m**(4+CP%sigma)/phi**CP%sigma
		else if (deriv==1) then
			Vofphi=CP%m**(4+CP%sigma)*CP%sigma*(-1._dl)/phi**(CP%sigma+1)
		else if (deriv==2) then
			Vofphi=CP%m**(4+CP%sigma)*CP%sigma*(CP%sigma+1)/phi**(CP%sigma+2)
		else 
			stop 'Invalid deriv in Vofphi'
		end if
	else if(CP%pot_type==5) then  !FNW, CP%sigma = mu
		mu=CP%sigma
		ma=amnu/avarmnu(phi,0)
		dmadphi=amnu*(-1._dl)*avarmnu(phi,1)/avarmnu(phi,0)**2
		if (deriv==0) then
			VofPhi=CP%m**4*log(1._dl+ma/mu)
		else if (deriv==1) then
			VofPhi=CP%m**4*dmadphi/(mu*(1+ma/mu))
		else if (deriv==2) then
			ddmaddphi=2._dl*amnu*avarmnu(phi,1)**2/avarmnu(phi,0)**3-amnu*avarmnu(phi,2)/avarmnu(phi,0)**2
			VofPhi=CP%m**4*ddmaddphi/(mu*(1+ma/mu))-CP%m**4*dmadphi**2/(mu**2*(1+ma/mu)**2)
		else 
			stop 'Invalid deriv in Vofphi'
		end if
		stop 'Invalid potential selected...'
	end if

end  function Vofphi

subroutine EvolveBackground(dum,num,a,y,yprime)
	use MassiveNu
	implicit none
	real dum
	integer num
	real(dl) tau, y(num),yprime(num)
	real(dl) a, a2, tot
	real(dl) phi,dphida, tmp, phidot,rhonu,pnu
	real(dl),parameter::cant=2.99792458e8_dl,Gant=6.6726e-11,eightpi=25.13274123_dl,Mpcant=3.085678e22_dl


	a2=a**2
	phi = y(1)
	phidot = y(2)/a2
	! write(*,*)'Evolve phi: ',phi
	!write(*,*)'mass: ',avarmnu(phi,0)
	call Nu_background(avarmnu(phi,0)*a,rhonu,pnu)
	tmp=a2*(0.5d0*phidot**2 + a2*Vofphi(phi,0))
	tot=(grhoc+grhob)*a+grhog+grhornomass +grhok*a2 + tmp +grhormass*rhonu   !Antz
	adot=sqrt(tot/3.0d0)
	dphida=phidot/adot
	yprime(1)=dphida
	!Antz
	!          yprime(2)= -a2**2*(Vofphi(phi,1)+avarmnu(phi,1)*(grhormass)*(rhonu-3*pnu)/(avarmnu(phi,0)*a**4*sqrt(eightpi*Gant)))/adot

	!equatino for beta unitless...
	yprime(2)= -a2**2*(Vofphi(phi,1)+avarmnu(phi,1)*(grhormass)*(rhonu-3*pnu)/(avarmnu(phi,0)*a**4))/adot

	!     yprime(2)= -a2**2*(Vofphi(phi,1)+avarmnu(phi,1)*(grhormass)*(rhonu-3*pnu)/(avarmnu(phi,0)*a**4)-2*phidot*adot/a**3)/adot
	!     eom with damping term removed

	!         write(*,*)'original : ',Vofphi(phi,1)
	!         write(*,*)'beta: ',avarmnu(phi,1)/avarmnu(phi,0)
	!         write(*,*)'new: ',avarmnu(phi,1)*grhormass*(rhonu-3*pnu)/(avarmnu(phi,0)*a**4*sqrt(eightpi*Gant))
	!         write(*,*)'v :',(Vofphi(phi,1)-avarmnu(phi,1)*(grhormass)*(rhonu-3*pnu)/(avarmnu(phi,0)*a**4*sqrt(eightpi*Gant)))
	!         write(*,*)'***************************************'
	!         read(*,*)

end subroutine EvolveBackground


function Quint_GetOmegaFromInitial(astart,phi,phidot,atol)
	!Get CP%omegav today given particular conditions phi and phidot at a=astart
	use MassiveNu
	real(dl), intent(IN) :: astart, phi,phidot, atol
	real(dl) Quint_GetOmegaFromInitial
	integer, parameter ::  NumEqs=2
	real(dl) c(24),w(NumEqs,9), y(NumEqs), ast
	integer ind, i
	real dum
	real(dl) rhonuant,pnuant,rhotot


	ast=astart
	ind=1
	y(1)=phi
	y(2)=phidot*astart**2  !Fixed Dec 02
	call dverk(dum,NumEqs,EvolveBackground,ast,y,1._dl,atol,ind,c,NumEqs,w)
	call EvolveBackground(dum,NumEqs,1._dl,y,w(:,1))
	rhotot=(CP%h0)**2*3d6/cx**2
	!           Quint_GetOmegaFromInitial=(0.5d0*y(2)**2 + Vofphi(y(1),0))/(3*adot**2)
	Quint_GetOmegaFromInitial=(0.5d0*y(2)**2 + Vofphi(y(1),0))/rhotot
	tempphi=y(1)
	tempphidot=y(2)
	call Nu_background(avarmnu(tempphi,0),rhonuant,pnuant)
	!           omnuant=rhonuant*grhormass/(3*adot**2)
	if (ind /= 3) then
		!              write(*,*)'error? ',ind
		!              read(*,*)
		Quint_GetOMegaFromInitial=1._dl
		!           else
		!              write(*,*)'Ookilly doakily'
	end if
end function Quint_GetOmegaFromInitial


subroutine Quint_init_background
	use MassiveNu
	real(dl) astart,aend, afrom
	integer, parameter ::  NumEqs=2
	real(dl) c(24),w(NumEqs,9), y(NumEqs),atol
	integer ind, i, iter, jimbob,jiter  !Antz
	real(dl) aVals(NumPointsEx), splZero
	real(dl) om1,om2, phi, deltaphi, om, initial_mu,initial_mu2,delta_mu
	logical OK,masscheck
	real dum
	real(dl) effint, omint, p, rhofrac,tempp,tempaa,jump,tempmnu,tempmnu2
	integer::stat           
	! filenames for writing background (GNQ simulation)
	character (LEN = 100) :: H_name, q_name, qdot_name
	character (LEN = 100) :: rho_cdm_name, rho_nu_name, p_nu_name
	integer, parameter :: H_file = 13, q_file = 14, qdot_file = 15
	integer, parameter :: rho_cdm_file = 16, rho_nu_file = 17, p_nu_file = 18
	real(dl) H_tmp, rho_phi_tmp, rho_nu_tmp, p_nu_tmp, rho_crit_tmp

	!Make interpolation table, etc, 
	!At this point massive neutrinos have been initialized
	!so nu1 can be used to get their density and pressure at scale factor a
	!Other built-in components have density and pressure scaling trivially with a

	!These two must bracket the correct value to give CP%omegav today
	!Assume that higher initial phi gives higher CP%omegav today
	!Can fix initial_phi to correct value
	CP%pot_type=1
	OK = .true.
	call setuservals        !Antz old beta setting routine
	astart=1d-12
	atol=1d-8
	!write(*,*)'initial_phi: ',CP%initial_phi
	!write(*,*)'initial m: ',CP%m
	phi=CP%initial_phi
	if (.true.) then

		if (CP%pot_type==1) then
			!Antz if using exp pot then use this:
			!call mansetmphi(OK,CP%m)  !TESTING ONLY
			!write(*,*)'comment out the previous line!'
			!stop
			!call setmphi(OK)
			!Antz else search for phi
		else if ((CP%pot_type==2).or.(CP%pot_type==3).or.(CP%pot_type==4)) then
			write(*,*)'hello'
			call setmphi(OK)
			!write(*,*)'amnu: dubbydo: ',amnu
			!  call findphi(phi,OK)
			!call manfindphi(phi,OK) !TESTING ONLY 
			CP%initial_phi=phi
		else if (CP%pot_type==5) then 
			write(*,*)'continuing....'

		else
			stop 'invalid choice of potential type '
		end if
		!write(*,*)'pot type: ',CP%pot_type
		!write(*,*)'initial phi: ',CP%initial_phi
		!write(*,*)'sigma: ',CP%sigma
		!write(*,*)'m: ',CP%m
	else

		do jiter=1,40
		om = Quint_GetOmegaFromInitial(astart,phi,0._dl,atol)
		call setvarmnu(tempphi,tempphidot,masscheck)

		if (abs(tempaa-om)<1d-4) then 
			!                write(*,*)'i converged...'
			exit
		end if
		tempaa=om
		end do

		if ((abs(om1-CP%omegav) > 1d-3) .or. (abs(omnuant-CP%omegan)>1d-3)) then  !Antz
			!if not, do binary search in the interval
			OK=.false.

			jump=CP%fractemp
			!write(*,*)'initial phi: ',CP%initial_phi
			if (om<CP%omegan) then
				do iter=1,1000
				write(*,*)'phi: ',phi
				tempaa=1000._dl
				tempmnu2=tempmnu
				tempmnu=amnu
				do jiter=1,20
				call setvarmnu(tempphi,tempphidot,masscheck)
				om = Quint_GetOmegaFromInitial(astart,phi,0._dl,atol)
				if (abs(om-tempaa)<1d-3) then
					!                write(*,*)'converged...'
					exit
				end if
				tempaa=om
				end do

				if (abs(om-CP%omegav)<1d-3) then  !antz
					OK=.true.
					exit
				end if
				if (om>CP%omegav) then 
					phi=phi-jump
					jump=jump/10.
					phi=phi-jump
					amnu=tempmnu2
				end if
				phi=phi+jump
				write(*,*)'omegav:  ',om,'   omegan: ',getomeganunow(tempphi)
				write(*,*)'CP omegav: ',CP%omegav,'    CP omegan:  ',CP%omegan
				write(*,*)'new omega nu: ',omnuant
				write(*,*)'================================================='
				read(*,*)
				end do
			else
				do iter=1,1000
				write(*,*)'phi: ',phi
				tempaa=1000._dl
				tempmnu2=tempmnu
				tempmnu=amnu
				!write(*,*)'amnu: ',amnu


				do jiter=1,20
				call setvarmnu(tempphi,tempphidot,masscheck)
				om = Quint_GetOmegaFromInitial(astart,phi,0._dl,atol)
				if ((abs(om-tempaa)<1d-3).and.masscheck) then
					!               write(*,*)'converged...'
					exit
				end if
				tempaa=om
				end do

				if ((abs(om-CP%omegav)<1d-3).and.masscheck) then  !antz
					OK=.true.
					exit
				end if
				if (om<CP%omegav) then 
					phi=phi-jump
					jump=jump/10.
					phi=phi-jump
					amnu=tempmnu2
				end if
				phi=phi+jump
				write(*,*)'omegav:  ',om,'   omegan: ',getomeganunow(tempphi)
				write(*,*)'CP omegav: ',CP%omegav,'    CP omegan:  ',CP%omegan
				write(*,*)'new omega nu: ',omnuant
				write(*,*)'================================================='
				read(*,*)
				end do !iterations

			end if


		end if
	end if
	if (.not. OK) then
		write(*,*) 'Search for good intial conditions did not converge' !this shouldn't happen
		converge=.false.
		CP%universeok=.false.
		!call mansetmphi(converge,phi)
	else
		converge=.true.
		CP%universeok=.true.
	end if
	!amnu=5080.6936
	!phi=-0.660387469

	!amnu=5595.55239
	!phi=-2.37240853

	!Find initial
	if (.not.doconvergence.and.CP%universeok) then
		!om = Quint_GetOmegaFromInitial(astart,phi,0._dl,atol)

		!           write(*,*)'User set omegav: ',CP%omegav,'   Converged value: ',om
		!           write(*,*)'User set omegan: ',CP%omegan,'   Converged value: ',getomeganunow(tempphi)
		!           write(*,*)'new omenanu: ',omnuant
		!Antz
		!
		!         write(*,*)'Found initial phi...',phi,'  ..phi now: ',tempphi
		!        write(*,*)'Mass const: ',amnu,'  mass now: ',avarmnu(tempphi,0),' = ',avarmnu(tempphi,0)*1.68e-4,'eV'
		!read(*,*)
		!Antz          


		atol=1d-8
		ind=1
		!y(1)=astart/adotrad
		y(1)=phi
		y(2)=0
		!   write(*,*)'tester: ',y(1),y(2)        
		!   stop
		phi_a(1)=y(1)
		phidot_a(1)=y(2)/astart**2
		afrom=astart
		da=1._dl/(NumPoints-1)  !phi_a(NumPoints) is value now
		aVals(1)=astart
		!!Might need to be more careful about first point

		effint = 0 ; omint = 0
		do i=1, NumPointsEx-1 
		aend = da*i
		aVals(i+1)=aend
		call dverk(dum,NumEqs,EvolveBackground,afrom,y,aend,atol,ind,c,NumEqs,w)
		if (.not.(ind)==3) then
			CP%universeOK=.false.
			!stop 'bad dverk'
			open(21,file='lastdverkerror.dat')
			write(21,*)'last value of ind: ',ind
			write(*,*)'last value of ind: ',ind
			write(*,*)'params: ',CP%beta, CP%sigma, CP%h0
			write(*,*)'atol: ',atol
			write(*,*)'$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$'
			close(21)
			exit
		end if
		call EvolveBackground(dum,NumEqs,aend,y,w(:,1))
		phi_a(i+1)=y(1)
		phidot_a(i+1)=y(2)/aend**2
		p = phidot_a(i+1)**2/2 - aend**2*Vofphi(phi_a(i+1),0)
		rhofrac = 3*(adot/aend)**2
		effint = effint + p/rhofrac
		omint = omint + (phidot_a(i+1)**2 -p)/rhofrac

		if (i==NumPoints-1) then
			if (FeedbackLevel > 0) then
				!                write(*,*) 'Omega_Q_0=',real((0.5d0*phidot_a(i+1)**2 + Vofphi(phi_a(i+1),0))/(3*adot**2)), &
				!                   '  w_0=',real((0.5d0*phidot_a(i+1)**2 - Vofphi(phi_a(i+1),0))/(0.5d0*phidot_a(i+1)**2 + &
				!                      Vofphi(phi_a(i+1),0)))
				!             write (*,*) 'w_effective = ', real(effint/omint)
				!Antz
				!           write(*,*)'Phi now: ',aend,phi_a(i+1)
				!Antz
			end if
		end if
		end do
		if (CP%universeOK) then

			! Write background for GNQ simulation

			if (CP%write_background) then
				! Create filenames from root
				H_name = 'output/bg_' // trim(CP%root_filename) // '_H.dat'
				q_name = 'output/bg_' // trim(CP%root_filename) // '_q.dat'
				qdot_name = 'output/bg_' // trim(CP%root_filename) // '_qdot.dat'
				rho_cdm_name = 'output/bg_' // trim(CP%root_filename) // '_rho_cdm.dat'
				rho_nu_name = 'output/bg_' // trim(CP%root_filename) // '_rho_nu.dat'
				p_nu_name = 'output/bg_' // trim(CP%root_filename) // '_p_nu.dat'

				open (H_file, file = trim(H_name), status = 'replace')
				open (q_file, file = trim(q_name), status = 'replace')
				open (qdot_file, file = trim(qdot_name), status = 'replace')
				open (rho_cdm_file, file = trim(rho_cdm_name), status = 'replace')
				open (rho_nu_file, file = trim(rho_nu_name), status = 'replace')
				open (p_nu_file, file = trim(p_nu_name), status = 'replace')

				do i = 5, NumPointsEx - 1
					! Calculate necessary quantities; be careful with various
					! powers of a (somewhat strange conventions).

					call Nu_background(avarmnu(phi_a(i), 0) * aVals(i), &
							rho_nu_tmp, p_nu_tmp)
					rho_phi_tmp = ( aVals(i)**2 * (0.5d0 * phidot_a(i)**2 &
							+ aVals(i)**2 * Vofphi(phi_a(i), 0)) ) / aVals(i)**4
					rho_crit_tmp = (grhoc + grhob) * aVals(i) + grhog &
							+ grhornomass + grhok * aVals(i)**2 &
							+ rho_phi_tmp * aVals(i)**4 + grhormass * rho_nu_tmp
					H_tmp = dsqrt(1.d0 / 3.d0 * rho_crit_tmp) / aVals(i)

					! Write to files

					write (H_file, *) aVals(i), H_tmp ! this is H_curl = Ha
					write (q_file, *) aVals(i), phi_a(i)
					write (qdot_file, *) aVals(i), phidot_a(i) &
							* 1.d0 / (H_tmp * aVals(i)) ! gives dphi/da
					write (rho_cdm_file, *) aVals(i), (grhoc + grhob) &
							/ aVals(i)**3
					write (rho_nu_file, *) aVals(i), grhormass * rho_nu_tmp &
							/ aVals(i)**4
					write (p_nu_file, *) aVals(i), grhormass * p_nu_tmp &
							/ aVals(i)**4
				end do

				close (H_file)
				close (q_file)
				close (qdot_file)
				close (rho_cdm_file)
				close (rho_nu_file)
				close (p_nu_file)
			end if

			! Create background splines

			splZero=0
			call spline(aVals,phi_a,NumPointsEx,splZero,splZero,ddphi_a)
			call spline(aVals,phidot_a,NumPointsEx,splZero,splZero,ddphidot_a)

		end if

		!            call resultant
	end if


end subroutine Quint_init_background

subroutine Quint_ValsAta(a,aphi,aphidot)
	implicit none
	!Do interpolation for phi and phidot at a
	real(dl) a, aphi, aphidot
	real(dl) a0,b0,ho2o6,a03,b03

	integer ix
	ix = int(a/da)+1
	a0 = (ix*da -a)/da
	b0 = (a-(ix-1)*da)/da
	ho2o6 = da**2/6._dl
	a03=(a0**3-a0)
	b03=(b0**3-b0)
	aphi=a0*phi_a(ix)+b0*phi_a(ix+1)+(a03*ddphi_a(ix)+b03*ddphi_a(ix+1))*ho2o6
	aphidot=a0*phidot_a(ix)+b0*phidot_a(ix+1)+(a03*ddphidot_a(ix)+b03*ddphidot_a(ix+1))*ho2o6
end subroutine Quint_ValsAta





end module Quint





!Return OmegaK - modify this if you add extra fluid components
function GetOmegak()
	use precision
	use Quint
	use ModelParams
	real(dl)  GetOmegak

	GetOmegak = 1 - (CP%omegab+CP%omegac+CP%omegav+CP%omegan)     !(0.01/(CP%H0/100)**2))    Antz+CP%omegan) 

end function GetOmegak


subroutine init_background
	use Quint
	implicit none
	!This is only called once per model, and is a good point to do any extra initialization.
	!It is called before first call to dtauda, but after
	!massive neutrinos are initialized and after GetOmegak
	if (doconvergence) then
		call checkconverge
		stop
	end if

	if (has_extra) call Quint_init_background
end  subroutine init_background


!Background evolution
function dtauda(a)
	!get d tau / d a
	use precision
	use ModelParams
	use MassiveNu
	use Quint
	implicit none
	real(dl) dtauda
	real(dl), intent(IN) :: a
	real(dl) rhonu,pnu,grhoa2, a2, phi, phidot

	a2=a**2

	!  8*pi*G*rho*a**4.
	grhoa2=grhok*a2 +(grhoc+grhob)*a+grhog+grhornomass
	call Quint_ValsAta(a,phi,phidot)           !Antz see below
	if (CP%Num_Nu_massive /= 0) then
		!Get massive neutrino density relative to massless
		call Nu_background(a*avarmnu(phi,0),rhonu,pnu)
		grhoa2=grhoa2+rhonu*grhormass
	end if

	!Get any extra contributions
	if (has_extra) then
		!  call Quint_ValsAta(a,phi,phidot)    Antz moved so called before avarmnu
		grhoa2=grhoa2 + a2*(phidot**2/2 + a2*Vofphi(phi,0))
	else
		grhoa2=grhoa2 +grhov*a2**2
	end if

	dtauda=sqrt(3/grhoa2)

end function dtauda
!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc

!Gauge-dependent perturbation equations

module GaugeInterface
use precision
use ModelParams
use MassiveNu
use Quint
use LambdaGeneral
implicit none
public

!Description of this file. Change if you make modifications.
character(LEN=*), parameter :: Eqns_name = 'gauge_quint'

logical  :: DoTensorNeutrinos = .false.


real(dl) vec_sig0,  Magnetic !(dummies)
integer, parameter :: max_l_evolve = 100 !50  !Maximum l we are ever likely to propagate

!Supported scalar initial condition flags
integer, parameter :: initial_adiabatic=1, initial_iso_CDM=2, &
	initial_iso_baryon=3,  initial_iso_neutrino=4, initial_iso_neutrino_vel=5, initial_iso_quint=6,initial_vector = 0
integer, parameter :: initial_nummodes = initial_iso_quint


type EvolutionVars
	real(dl) q, q2
	real(dl) k_buf,k2_buf ! set in initial

	integer w_ix !Index of two quintessence equations

	integer q_ix !index into q_evolve array that gives the value q
	logical TransferOnly

	!       nvar (t) - number of scalar (tensor) equations for this k       
	integer nvar,nvarv, nvart

	!Max_l for the various hierarchies
	integer lmaxg,lmaxnr,lmaxnu,lmaxgpol,MaxlNeeded
	integer lmaxnrt, lmaxnut, lmaxt, lmaxpolt
	integer lmaxnrv, lmaxv, lmaxpolv

	integer polind  !index into scalar array of polarization hierarchy


	!array indices for massive neutrino equations
	integer iq0,iq1,iq2

	!array index for tensor massive neutrino equations
	integer iqt

	!Buffers for non-flat vars
	real(dl) Kf(max_l_evolve),Kft(max_l_evolve)      
	!Initial values for massive neutrino v*3 variables calculated when switching 
	!to non-relativistic approx
	real(dl) G11,G30
	real(dl) w_nu !equation of state parameter for massive neutrinos
	!True when using non-relativistic approximation
	logical MassiveNuApprox

	!Tru when using tight-coupling approximation (required for stability at early times)

	logical TightCoupling

	!Numer of scalar equations we are propagating
	integer ScalEqsToPropagate
	!beta > l for closed models 
	integer FirstZerolForBeta
	!Tensor vars
	real(dl) tenspigdot, aux_buf

	real(dl) pig !For tight coupling

	real(dl) poltruncfac


end type EvolutionVars


!precalculated arrays
real(dl) polfac(max_l_evolve),tensfac(max_l_evolve),tensfacpol(max_l_evolve), &
	denl(max_l_evolve) 


real(dl), parameter :: ep0=1.0d-2
real(dl) epsw

integer debug

public GetNumEqns,output,outputt,initial,initialt,SwitchToMassiveNuApprox
public outtransf,GaugeInterface_EvolveScal
contains


subroutine GaugeInterface_EvolveScal(EV,tau,y,tauend,tol1,ind,c,w)
	use ThermoData
	type(EvolutionVars) EV
	real(dl) c(24),w(EV%nvar,9), y(EV%nvar), tol1, tau, tauend
	integer ind
	real(dl) ep, tau_switch, tau_check
	real(dl) cs2, opacity

	!Evolve equations from tau to tauend, performing switch-off of
	!tight coupling if neccessary.
	!In principle the tight coupling evolution routine could be different
	!which could probably be used to improve the speed a bit

	!It's possible due to instabilities that changing later is actually more
	!accurate, so cannot expect accuracy to vary monotonically with the switch

	if (EV%TightCoupling) then

		tau_switch = tight_tau !when 1/(opacity*tau) = 0.01ish

		!The numbers here are a bit of guesswork
		!The high k increase saves time for very small loss of accuracy
		!The lower k ones are more delicate. Nead avoid instabilities at same time
		!as ensuring tight coupling is accurate enough
		if (EV%k_buf > epsw) then
			if (EV%k_buf > epsw*5) then
				ep=ep0*5/AccuracyBoost
			else
				ep=ep0
			end if
		else
			ep=ep0 
		end if

		!Check the k/opacity criterion
		tau_check = min(tauend, tau_switch)   
		call thermo(tau_check,cs2,opacity)
		if (EV%k_buf/opacity > ep) then
			!so need to switch in this time interval
			tau_switch = Thermo_OpacityToTime(EV%k_buf/ep) 
		end if

		if (tauend > tau_switch) then 

			if (tau_switch > tau) then
				if (CP%flat) then
					call dverk(EV,EV%ScalEqsToPropagate,fderivs,tau,y,tau_switch,tol1,ind,c,EV%nvar,w)
				else
					call dverk(EV,EV%ScalEqsToPropagate,derivs,tau,y,tau_switch,tol1,ind,c,EV%nvar,w)
				end if
			end if

			!Set up variables with their tight coupling values
			y(8) = EV%pig
			y(EV%polind+2) = EV%pig/4         

			EV%TightCoupling = .false.

		end if
	end if

	if (CP%flat) then
		call dverk(EV,EV%ScalEqsToPropagate,fderivs,tau,y,tauend,tol1,ind,c,EV%nvar,w)
	else
		call dverk(EV,EV%ScalEqsToPropagate, derivs,tau,y,tauend,tol1,ind,c,EV%nvar,w)
	end if       


end subroutine GaugeInterface_EvolveScal



subroutine GaugeInterface_Init
	!Precompute various arrays
	integer j

	epsw = 100/CP%tau0

	if (CP%WantScalars) then
		do j=2,max_l_evolve
		polfac(j)=real((j+3)*(j-1),dl)/(j+1)
		end do               
	end if

	if (CP%WantTensors) then
		do j=2,max_l_evolve
		tensfac(j)=real((j+3)*(j-1),dl)/(j+1)
		tensfacpol(j)=tensfac(j)**2/(j+1)
		end do
	end if

	do j=1,max_l_evolve
	denl(j)=1._dl/(2*j+1)
	end do     

end subroutine GaugeInterface_Init


subroutine GetNumEqns(EV)
	use MassiveNu
	!Set the numer of equations in each hierarchy, and get total number of equations for this k
	type(EvolutionVars) EV
	real(dl) scal


	if (CP%Num_Nu_massive == 0) then
		EV%lmaxnu=0
	else 
		!l_max for massive neutrinos
		!  Antz           if (avarmnu(phi,0) < 2500 .and. EV%q > 0.02 ) then            Very light, so free-streaming more important             
		EV%lmaxnu=max(3,nint(6*lAccuracyBoost))   
		!  Antz           else
		!  Antz             EV%lmaxnu=max(3,nint(5*lAccuracyBoost))  
		!  Antz           end if
		if (CP%Transfer%high_precision) EV%lmaxnu=nint(25*lAccuracyBoost)
	end if

	if (CP%WantScalars) then
		EV%lmaxg  = max(nint(8*lAccuracyBoost),3)
		EV%lmaxnr = max(nint(7*lAccuracyBoost),3)
		EV%lmaxgpol = EV%lmaxg
		if (.not.CP%AccuratePolarization) EV%lmaxgpol=max(nint(4*lAccuracyBoost),3)

		if (EV%q < 0.05) then
			!Large scales need fewer equations
			scal  = 1
			if (CP%AccuratePolarization) scal = 4  !But need more to get polarization right
			EV%lmaxgpol=max(3,nint(min(8,nint(scal* 150* EV%q))*lAccuracyBoost))
			EV%lmaxnr=max(3,nint(min(7,nint(sqrt(scal)* 150 * EV%q))*lAccuracyBoost))
			EV%lmaxg=max(3,nint(min(8,nint(sqrt(scal) *300 * EV%q))*lAccuracyBoost))
		end if                  
		if (CP%Transfer%high_precision)  EV%lmaxnr=max(nint(25*lAccuracyBoost),3)
		EV%nvar=5+ (EV%lmaxg+1) + EV%lmaxgpol-1 +(EV%lmaxnr+1) 
		if (has_extra) then
			EV%w_ix = EV%nvar+1
			EV%nvar=EV%nvar+2
		end if

		if (CP%Num_Nu_massive /= 0) then
			if (CP%MassiveNuMethod == Nu_approx) then
				EV%iq0=EV%nvar + 1 
				EV%nvar= EV%nvar+(EV%lmaxnu+1)
			else
				EV%iq0=EV%nvar + 1 
				EV%iq1=EV%iq0+nqmax
				EV%iq2=EV%iq1+nqmax
				EV%nvar=EV%nvar+ nqmax*(EV%lmaxnu+1)
			endif
		end if

		EV%MaxlNeeded=max(EV%lmaxg,EV%lmaxnr,EV%lmaxgpol,EV%lmaxnu)
		if (EV%MaxlNeeded > max_l_evolve) stop 'Need to increase max_l_evolve'

		EV%poltruncfac=real(EV%lmaxgpol,dl)/(EV%lmaxgpol-2)
		EV%polind = 6+EV%lmaxnr+EV%lmaxg
		EV%lmaxt=0

	else
		EV%nvar=0
	end if

	if (CP%WantTensors) then
		EV%lmaxt=max(3,nint(8*lAccuracyBoost))
		EV%lmaxpolt = max(3,nint(5*lAccuracyBoost)) 
		if (EV%q < 0.05) then
			!Large scales need fewer equations
			scal  = 1
			if (CP%AccuratePolarization) scal = 4 !But need more to get polarization right
			EV%lmaxt=max(3,nint(min(8,nint(scal *150 * EV%q))*lAccuracyBoost))
			EV%lmaxpolt=max(3,nint(min(5,nint( scal*150 * EV%q))*lAccuracyBoost))
		end if      
		EV%nvart=(EV%lmaxt-1)+(EV%lmaxpolt-1)*2+3
		if (DoTensorNeutrinos) then

			EV%lmaxnrt=nint(6*lAccuracyBoost)
			if (EV%q < 0.05) EV%lmaxnrt = max(3,nint(min(6,nint(150 * EV%q))*lAccuracyBoost)) 
			EV%lmaxnut=nint(4*lAccuracyBoost)
			EV%nvart=EV%nvart+EV%lmaxnrt-1
			if (CP%Num_Nu_massive /= 0 ) then
				EV%iqt=EV%nvart + 1                  
				EV%nvart=EV%nvart+ nqmax*(EV%lmaxnut-1)         
			end if 
		end if
	else
		EV%nvart=0
	end if       

end subroutine GetNumEqns

!cccccccccccccccccccccccccccccccccc
subroutine SwitchToMassiveNuApprox(EV,y)
	!When the neutrinos are no longer highly relativistic we use a truncated
	!energy-integrated hierarchy going up to third order in velocity dispersion
	type(EvolutionVars) EV

	real(dl) a,a2,pnu,clxnu,dpnu,pinu,grhormass_t,grhonu_t,rhonu, phi,phidot,betatemp  !Antz added phi,phidot
	real(dl) shearnu, qnu
	real(dl) y(EV%nvar)

	a=y(1)
	a2=a*a

	grhormass_t=grhormass/a2
	call Quint_ValsAta(a,phi,phidot)          !Added to get phi for a to use in var mass

	!Get density and pressure as ratio to massles by interpolation from table
	call Nu_background(a*avarmnu(phi,0),rhonu,pnu)
	!Integrate over q
	betatemp=avarmnu(phi,1)/avarmnu(phi,0)
	call Nu_Integrate(a*avarmnu(phi,0),betatemp,rhonu,pnu,y(EV%w_ix),clxnu,qnu,dpnu,shearnu,y(EV%iq0),y(EV%iq1),y(EV%iq2))
	!clxnu_here  = rhonu*clxnu, qnu_here = qnu*rhonu
	!Could save time by only calculating shearnu in output
	dpnu=dpnu/rhonu
	qnu=qnu/rhonu
	clxnu = clxnu/rhonu
	pinu=1.5_dl*shearnu/rhonu                     


	grhonu_t=grhormass_t*rhonu

	EV%MassiveNuApprox=.true.

	y(EV%iq0)=clxnu
	y(EV%iq0+1)=dpnu
	y(EV%iq0+2)=qnu
	y(EV%iq0+3)=pinu

	call Nu_Intvsq(a*avarmnu(phi,0),EV%G11,EV%G30,y(EV%iq1),y(EV%iq0+3*nqmax))
	!Analytic solution for higher moments, proportional to a^{-3}
	EV%G11=EV%G11*a**3/rhonu  
	EV%G30=EV%G30*a**3/rhonu  

	EV%ScalEqsToPropagate=EV%iq0+3

end  subroutine SwitchToMassiveNuApprox


!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
subroutine output(EV,y, n,j,tau,sources)
	use ThermoData
	use lvalues
	use ModelData 


	implicit real(dl) (s-t)
	implicit logical (a-r,u-z)
	integer n,j
	type(EvolutionVars) EV
	real(dl), target :: y(n),yprime(n)
	real(dl), dimension(:),pointer :: ypol,ypolprime

	real(dl) dgq,grhob_t,grhor_t,grhoc_t,grhog_t,grhov_t,grhonu_t,gpnu_t,sigma,polter
	real(dl) qgdot,pigdot,pirdot,vbdot,dgrho
	real(dl) a,a2,z,clxc,clxb,vb,clxg,qg,pig,clxr,qr,pir,rhonu,pnu,pinu,clxnu,dpnu,qnu

	real(dl) tau,x,divfac,pinudot,rhonudot,shearnudot, grhormass_t
	real(dl) shearnu

	real(dl) k,k2  ,adotoa, grho, gpres,etak,phi, dgpi
	real(dl) clv, clvdot, phidot, grhoex_t,betatemp

	real(dl) sources(CTransScal%NumSources)



	if (CP%flat) then
		call fderivs(EV,EV%ScalEqsToPropagate,tau,y,yprime)
	else
		call derivs(EV,EV%ScalEqsToPropagate,tau,y,yprime)        
	end if

	ypolprime => yprime(EV%polind+1:)
	ypol => y(EV%polind+1:)

	k=EV%k_buf
	k2=EV%k2_buf

	if (EV%TightCoupling) then

		y(8) = EV%pig

		ypol(2) = EV%pig/4

	end if 


	a   =y(1)
	a2  =a*a
	etak=y(2)
	clxc=y(3)
	clxb=y(4)
	vb  =y(5)
	clxg=y(6)
	qg  =y(7)
	pig =y(8)
	clxr=y(7+EV%lmaxg)
	qr  =y(8+EV%lmaxg)
	pir =y(9+EV%lmaxg)
	pigdot=yprime(8)
	pirdot=yprime(EV%lmaxg+9)
	vbdot =yprime(5)
	qgdot =yprime(7)

	!  Compute expansion rate from: grho 8*pi*rho*a**2

	grhob_t=grhob/a
	grhoc_t=grhoc/a
	grhor_t=grhornomass/a2
	grhog_t=grhog/a2
	grhov_t=grhov*a2
	grhormass_t=grhormass/a2
	call Quint_ValsAta(a,phi,phidot)                  !antz moved from later call
	if (CP%Num_Nu_Massive == 0) then
		rhonu=1._dl
		pnu=1._dl/3._dl
		clxnu=0._dl
		pinu=0._dl
	else
		!Get density and pressure as ratio to massless by interpolation from table
		call Nu_background(a*avarmnu(phi,0),rhonu,pnu)

		if (CP%MassiveNuMethod == Nu_approx) then

			clxnu=y(EV%iq0)
			qnu=y(EV%iq0+1)
			pinu=y(EV%iq0+2)

		else

			if (EV%MassiveNuApprox) then    
				clxnu=y(EV%iq0)
				!dpnu = y(EV%iq0+1)
				qnu=y(EV%iq0+2)
				pinu=y(EV%iq0+3)
			else
				!Integrate over q
				betatemp=avarmnu(phi,1)/avarmnu(phi,0)
				call Nu_Integrate(a*avarmnu(phi,0),betatemp,rhonu,pnu,y(EV%w_ix),clxnu,qnu,dpnu,shearnu,y(EV%iq0),y(EV%iq1),y(EV%iq2))
				!clxnu_here  = rhonu*clxnu, qnu_here = qnu*rhonu
				!dpnu=dpnu/rhonu
				qnu=qnu/rhonu
				clxnu = clxnu/rhonu
				pinu=1.5_dl*shearnu/rhonu       
			endif
		end if

	end if

	EV%w_nu = pnu/rhonu


	grhonu_t=grhormass_t*rhonu
	gpnu_t=grhormass_t*pnu

	grho=grhob_t+grhoc_t+grhor_t+grhog_t+grhonu_t
	gpres=(grhog_t+grhor_t)/3 +gpnu_t


	if (has_extra) then

		! call Quint_ValsAta(a,phi,phidot)      Antz moved to earlier (before call to avarmnu
		grhoex_t=phidot**2/2 + a2*Vofphi(phi,0)
		grho=grho+grhoex_t
		gpres=gpres - grhoex_t+phidot**2     

	else
		grho=grho +grhov_t
		gpres=gpres - grhov_t
	end if

	adotoa=sqrt((grho+grhok)/3)


	!  8*pi*a*a*SUM[rho_i*clx_i]
	dgrho=grhob_t*clxb+grhoc_t*clxc + grhog_t*clxg+grhor_t*clxr +grhonu_t*clxnu

	!  8*pi*a*a*SUM[(rho_i+p_i)*v_i]
	dgq=grhob_t*vb+grhog_t*qg+grhor_t*qr+ grhonu_t*qnu

	if (has_extra) then
		clv=y(EV%w_ix)
		clvdot=y(EV%w_ix+1)     
		dgrho=dgrho + phidot*clvdot +clv*a2*Vofphi(phi,1)
		dgq=dgq + k*phidot*clv
	end if

	!  Get sigma (shear) and z from the constraints
	!  have to get z from eta for numerical stability       
	z=(0.5_dl*dgrho/k + etak)/adotoa 
	sigma=(z+1.5_dl*dgq/k2)/EV%Kf(1)

	polter = 0.1_dl*pig+9._dl/15._dl*ypol(2)

	if (CP%flat) then
		x=k*(CP%tau0-tau)
		divfac=x*x    
	else   
		x=(CP%tau0-tau)/CP%r
		divfac=(CP%r*rofChi(x))**2*k2 
	end if
	if (CP%Num_Nu_Massive == 0) then         
		pinudot=0._dl
	else
		if (CP%MassiveNuMethod == Nu_approx) then
			pinudot=yprime(EV%iq0+2)
		else
			if (EV%MassiveNuApprox) then
				pinudot=yprime(EV%iq0+3)
			else
				betatemp=avarmnu(phi,1)/avarmnu(phi,0)
				call Nu_derivs(a*avarmnu(phi,0),adotoa,betatemp,rhonu,pnu,phidot,rhonudot,shearnudot,y(EV%iq2),yprime(EV%iq2))

				!                 write(*,*)'rhonudot: ',rhonudot
				!                 write(*,*)'alternative2: ',adotoa*(4*rhonu-3*(rhonu+pnu))
				!                 write(*,*)'       ratio: ',rhonudot/(adotoa*(4*rhonu-3*(rhonu+pnu)))
				pinudot=1.5_dl*shearnudot/rhonu - rhonudot/rhonu*pinu    
			end if
		end if
	end if


	if (EV%TightCoupling) then
		dgpi = grhor_t*pir + grhonu_t*pinu + grhog_t*pig
		pigdot = -dopac(j)/opac(j)*pig + 32._dl/45*k/opac(j)*(-2*adotoa*sigma  &
			+etak/EV%Kf(1)-  dgpi/k +vbdot )
		ypolprime(2)= pigdot/4
	end if

	t5 = 1/adotoa
	t14 = 1/k
	t20 = grhor_t*pir
	t22 = grhonu_t*pinu
	t30 = k**2
	t31 = 1/t30

	s1 = 4.D0/3.D0*k*EV%Kf(1)*expmmu(j)*sigma+(-t5*dgrho/3+2.D0/3.D0*(- &
		sigma-t5*etak)*k+(gpres+5.D0/3.D0*grho)*sigma*t14+((3*pinu*gpnu_t+4* &
		grhog_t*pig+4*t20+3*t22)*adotoa-grhor_t*pirdot-grhonu_t*pinudot- &
		grhog_t*pigdot)*t31)*expmmu(j)+(clxg/4+3.D0/8.D0*ypol(2))*vis(j) 


	t101 = s1+(-21.D0/5.D0*adotoa*vis(j)*sigma+(3.D0/40.D0*qgdot-9.D0/ &
		80.D0*EV%Kf(2)*yprime(9)+vbdot-3.D0/8.D0*EV%Kf(2)*ypolprime(3))*vis(j)+ &
		11.D0/10.D0*sigma*dvis(j)+(vb+3.D0/40.D0*qg-3.D0/8.D0*EV%Kf(2)*ypol(3) &
		-9.D0/80.D0*EV%Kf(2)*y(9))*dvis(j))*t14+vis(j)*pig/16+((-9.D0/160.D0* &
		dvis(j)*opac(j)+(-21.D0/10.D0*grhog_t-9.D0/160.D0*dopac(j))*vis(j)&
		+3.D0/16.D0*ddvis(j))*pig+(3.D0/16.D0*pigdot-27.D0/80.D0*opac(j)* &
		ypol(2)+9.D0/8.D0*ypolprime(2))*dvis(j)+((-9.D0/160.D0*pigdot-27.D0 &
		/80.D0*ypolprime(2))*opac(j)-21.D0/10.D0*t22-21.D0/10.D0*t20-27.D0&
		/80.D0*dopac(j)*ypol(2))*vis(j)+9.D0/8.D0*ddvis(j)*ypol(2))*t31+(-&
		2*etak*adotoa*t14*expmmu(j)+21.D0/10.D0*etak*vis(j)*t14)/EV%Kf(1)


	sources(1)=t101


	if (x > 0._dl) then
		!E polarization source
		sources(2)=vis(j)*polter*(15._dl/8._dl)/divfac 
		!factor of four because no 1/16 later
	else
		sources(2)=0
	end if

	if (CTransScal%NumSources > 2) then
		!Get lensing sources
		!Can modify this here if you want to get power spectra for other tracer
		if (tau>taurend .and. CP%tau0-tau > 0.1_dl) then

			!phi_lens = phi - 1/2 kappa (a/k)^2 sum_i rho_i pi_i

			!phi= -(dgrho +3*dgq*adotoa/k)/(k2)
			!write(*,*)'phi initial: ',phi
			!phi = etak/k-adotoa*sigma
			!write(*,*)'phi new: ',phi
			!write(*,*)'-------------------------------'
			!read(*,*)             !Antz


			phi = -(dgrho +3*dgq*adotoa/k)/(k2*EV%Kf(1)*2)  - (grhor_t*pirdot + grhog_t*pig+ pinu*gpnu_t)/k2



			sources(3) = -2*expmmu(j)*phi*(tau-tau_maxvis)/((CP%tau0-tau_maxvis)*(CP%tau0-tau))

			!We include the lensing factor of two here
		else
			sources(3) = 0
		end if
	end if

end subroutine output


!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
subroutine outputt(EV,yt,n,j,tau,dt,dte,dtb)
	!calculate the tensor sources for open and closed case
	use ThermoData

	implicit none
	integer j,n
	type(EvolutionVars) :: EV
	real(dl), target :: yt(n), ytprime(n)
	real(dl) tau,dt,dte,dtb,x,polterdot,polterddot,prefac
	real(dl) pig, pigdot, aux, polter, shear
	real(dl) sinhxr,cothxor
	real(dl) k,k2
	real(dl), dimension(:),pointer :: E,Bprime,Eprime
	stop 'not fixed for mavans....'
	if (CP%flat) then
		call fderivst(EV,EV%nvart,tau,yt,ytprime)
	else
		call derivst(EV,EV%nvart,tau,yt,ytprime)
	end if


	k2=EV%k2_buf
	k=EV%k_buf 
	pigdot=EV%tenspigdot
	pig=yt(4)
	aux=EV%aux_buf  
	shear = yt(3)

	x=(CP%tau0-tau)/CP%r

	sinhxr=rofChi(x)*CP%r

	if (EV%q*sinhxr > 1.e-8_dl) then  

		prefac=sqrt(EV%q2*CP%r*CP%r-CP%Ksign)
		cothxor=cosfunc(x)/sinhxr
		E => yt(EV%lmaxt+3-1:)   
		Eprime=> ytprime(EV%lmaxt+3-1:) 

		Bprime => Eprime(EV%lmaxpolt:)

		polter = 0.1_dl*pig + 9._dl/15._dl*E(2)
		polterdot=9._dl/15._dl*Eprime(2) + 0.1_dl*pigdot
		polterddot = 9._dl/15._dl*(-dopac(j)*(E(2)-polter)-opac(j)*(  &
			Eprime(2)-polterdot) + k*(2._dl/3._dl*Bprime(2)*aux - &
			5._dl/27._dl*Eprime(3)*EV%Kft(2))) &
			+0.1_dl*(k*(-ytprime(5)*EV%Kft(2)/3._dl + 8._dl/15._dl*ytprime(3)) - &
			dopac(j)*(pig - polter) - opac(j)*(pigdot-polterdot))

		dt=(shear*expmmu(j) + (15._dl/8._dl)*polter*vis(j)/k)*CP%r/sinhxr**2/prefac 

		dte=CP%r*15._dl/8._dl/k/prefac* &
			((ddvis(j)*polter + 2._dl*dvis(j)*polterdot + vis(j)*polterddot)  &
			+ 4._dl*cothxor*(dvis(j)*polter + vis(j)*polterdot) - &
			vis(j)*polter*(k2 -6*cothxor**2))

		dtb=15._dl/4._dl*EV%q*CP%r/k/prefac*(vis(j)*(2._dl*cothxor*polter + polterdot) + dvis(j)*polter)

	else
		dt=0._dl
		dte=0._dl
		dtb=0._dl
	end if

end subroutine outputt



!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
subroutine initial(EV,y, tau)
	!  Initial conditions.
	implicit none
	type(EvolutionVars) EV
	real(dl) y(EV%nvar)
	real(dl) Rv,Rg,Rp15,tau,x,x2,x3,aj3r,om,omtau,eta,Rc,Rb,grhonu,chi,phi,iqg
	real(dl) k,k2 
	real(dl) a,a2,clxc,clxg,clxr,clxb,qg,qr,vb,pir
	integer l,i
	integer, parameter :: i_clxg=1,i_clxr=2,i_clxc=3, i_clxb=4, &
		i_qg=5,i_qr=6,i_vb=7,i_pir=8, i_eta=9, i_aj3r=10,i_clv=11,i_clvdot=12
	integer, parameter :: i_max = i_clvdot
	real(dl) initv(6,1:i_max), initvec(1:i_max)


	EV%ScalEqsToPropagate=EV%nvar
	if (CP%flat) then
		EV%k_buf=EV%q
		EV%k2_buf=EV%q2     
		EV%Kf(1:EV%MaxlNeeded)=1._dl !initialize for CP%flat case 
	else
		EV%k2_buf=EV%q2-CP%curv
		EV%k_buf=sqrt(EV%k2_buf)

		do l=1,EV%MaxlNeeded
		EV%Kf(l)=1._dl-CP%curv*(l*(l+2))/EV%k2_buf           
		end do
	end if


	k=EV%k_buf
	k2=EV%k2_buf

	if (CP%closed) then
		EV%FirstZerolForBeta = nint(EV%q*CP%r) 
	else 
		EV%FirstZerolForBeta=l0max !a large number
	end if




	!  k*tau, (k*tau)**2, (k*tau)**3
	x=k*tau
	x2=x*x
	x3=x2*x
	grhonu=grhormass+grhornomass

	om = (grhob+grhoc)/sqrt(3*(grhog+grhonu))       
	omtau=om*tau
	Rv=grhonu/(grhonu+grhog)
	Rg = 1-Rv
	Rc=CP%omegac/(CP%omegac+CP%omegab)
	Rb=1-Rc
	Rp15=4*Rv+15


	if (CP%Scalar_initial_condition > initial_nummodes) &
		stop 'Invalid initial condition for scalar modes'


	a=tau*adotrad*(1+omtau/4)
	a2=a*a

	initv=0

	!  Set adiabatic initial conditions

	chi=1  !Get transfer function for chi
	initv(1,i_clxg)=-chi*EV%Kf(1)/3*x2*(1-omtau/5)
	initv(1,i_clxr)= initv(1,i_clxg)
	initv(1,i_clxb)=0.75_dl*initv(1,i_clxg)
	initv(1,i_clxc)=initv(1,i_clxb)
	initv(1,i_qg)=initv(1,i_clxg)*x/9._dl
	initv(1,i_qr)=-chi*EV%Kf(1)*(4*Rv+23)/Rp15*x3/27
	initv(1,i_vb)=0.75_dl*initv(1,i_qg)
	initv(1,i_pir)=chi*4._dl/3*x2/Rp15*(1+omtau/4*(4*Rv-5)/(2*Rv+15))
	initv(1,i_aj3r)=chi*4/21._dl/Rp15*x3
	initv(1,i_eta)=-chi*2*EV%Kf(1)*(1 - x2/12*(-10._dl/Rp15 + EV%Kf(1)))

	if (CP%Scalar_initial_condition/= initial_adiabatic) then
		!CDM isocurvature   

		initv(2,i_clxg)= Rc*omtau*(-2._dl/3 + omtau/4)
		initv(2,i_clxr)=initv(2,i_clxg)
		initv(2,i_clxb)=initv(2,i_clxg)*0.75_dl
		initv(2,i_clxc)=1+initv(2,i_clxb)
		initv(2,i_qg)=-Rc/9*omtau*x
		initv(2,i_qr)=initv(2,i_qg)
		initv(2,i_vb)=0.75_dl*initv(2,i_qg)
		initv(2,i_pir)=-Rc*omtau*x2/3/(2*Rv+15._dl)
		initv(2,i_eta)= Rc*omtau*(1._dl/3 - omtau/8)*EV%Kf(1)
		initv(2,i_aj3r)=0
		!Baryon isocurvature
		if (Rc==0) stop 'Isocurvature initial conditions assume non-zero dark matter'

		initv(3,:) = initv(2,:)*(Rb/Rc)
		initv(3,i_clxc) = initv(3,i_clxb)
		initv(3,i_clxb)= initv(3,i_clxb)+1

		!neutrino isocurvature density mode

		initv(4,i_clxg)=Rv/Rg*(-1 + x2/6)
		initv(4,i_clxr)=1-x2/6
		initv(4,i_clxc)=-omtau*x2/80*Rv*Rb/Rg
		initv(4,i_clxb)= Rv/Rg/8*x2
		iqg = - Rv/Rg*(x/3 - Rb/4/Rg*omtau*x)
		initv(4,i_qg) =iqg
		initv(4,i_qr) = x/3
		initv(4,i_vb)=0.75_dl*iqg
		initv(4,i_pir)=x2/Rp15
		initv(4,i_eta)=EV%Kf(1)*Rv/Rp15/3*x2
		initv(6,i_aj3r)=0

		!neutrino isocurvature velocity mode

		initv(5,i_clxg)=Rv/Rg*x - 2*x*omtau/16*Rb*(2+Rg)/Rg**2
		initv(5,i_clxr)=-x -3*x*omtau*Rb/16/Rg
		initv(5,i_clxc)=-9*omtau*x/64*Rv*Rb/Rg
		initv(5,i_clxb)= 3*Rv/4/Rg*x - 9*omtau*x/64*Rb*(2+Rg)/Rg**2
		iqg = Rv/Rg*(-1 + 3*Rb/4/Rg*omtau+x2/6 +3*omtau**2/16*Rb/Rg**2*(Rg-3*Rb))
		initv(5,i_qg) =iqg
		initv(5,i_qr) = 1 - x2/6*(1+4*EV%Kf(1)/(4*Rv+5))
		initv(5,i_vb)=0.75_dl*iqg
		initv(5,i_pir)=2*x/(4*Rv+5)+omtau*x*6/Rp15/(4*Rv+5)
		initv(5,i_eta)=2*EV%Kf(1)*x*Rv/(4*Rv+5) + omtau*x*3*EV%Kf(1)*Rv/32*(Rb/Rg - 80/Rp15/(4*Rv+5))
		initv(5,i_aj3r) = 3._dl/7*x2/(4*Rv+5)

		!quintessence isocurvature mode

		initv(6,i_clv) = (1 - x2/6+omtau*x2/72)
		initv(6,i_clvdot) = k*(-x/3 + omtau*x/24)

	end if


	if (CP%Scalar_initial_condition==initial_vector) then
		InitVec = 0

		do i=1,initial_nummodes
		InitVec = InitVec+ initv(i,:)*CP%InitialConditionVector(i)
		end do
	else
		InitVec = initv(CP%Scalar_initial_condition,:)
		if (CP%Scalar_initial_condition==initial_adiabatic) InitVec = -InitVec
		!So we start with chi=-1 as before
	end if

	y(1)=a
	y(2)= -InitVec(i_eta)*k/2
	!get eta_s*k, where eta_s is synchronous gauge variable

	!  CDM
	y(3)=InitVec(i_clxc)

	!  Baryons
	y(4)=InitVec(i_clxb)
	y(5)=InitVec(i_vb)

	!  Photons
	y(6)=InitVec(i_clxg)
	y(7)=InitVec(i_qg) 


	y(8:EV%nvar)=0._dl

	if (has_extra) then
		y(EV%w_ix) = InitVec(i_clv)
		!          y(EV%w_ix)=1d-4     !Antz
		y(EV%w_ix+1) = InitVec(i_clvdot)

	end if       

	!  Neutrinos
	y(7+EV%lmaxg)=InitVec(i_clxr)
	y(8+EV%lmaxg)=InitVec(i_qr)
	y(9+EV%lmaxg)=InitVec(i_pir)

	if (EV%FirstZerolForBeta==3) then
		y(10+EV%lmaxg)=0._dl
	else
		y(10+EV%lmaxg)=InitVec(i_aj3r)
	endif

	EV%TightCoupling=.true.


	EV%MassiveNuApprox=.false.
	if (CP%Num_Nu_massive == 0) return 

	if (CP%MassiveNuMethod == Nu_approx) then
		!Values are just same as massless neutrino ones since highly relativistic
		y(EV%iq0)=InitVec(i_clxr)
		y(EV%iq0+1)=InitVec(i_qr)
		y(EV%iq0+2)=InitVec(i_pir)
		if (EV%FirstZerolForBeta/=3) then
			y(EV%iq0+3)=InitVec(i_aj3r)
		endif
	else
		do  i=1,nqmax
		y(EV%iq0+i-1)=-0.25_dl*dlfdlq(i)*InitVec(i_clxr)
		y(EV%iq1+i-1)=-0.25_dl*dlfdlq(i)*InitVec(i_qr)
		y(EV%iq2+i-1)=-0.25_dl*dlfdlq(i)*InitVec(i_pir)
		end do       
	end if  


end subroutine initial


!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
subroutine initialt(EV,yt,tau)
	!  Initial conditions for tensors

	implicit none
	real(dl) bigR,tau,x,aj3r,elec, pir
	integer l
	type(EvolutionVars) EV
	real(dl) k,k2 ,a
	real(dl) yt(EV%nvart)

	stop 'nof fixed for mavans2'
	if (CP%flat) then
		EV%aux_buf=1._dl
		EV%k2_buf=EV%q2
		EV%k_buf=EV%q       
		EV%Kft(1:EV%lmaxt)=1._dl!initialize for CP%flat case
	else

		EV%k2_buf=EV%q2-3*CP%curv
		EV%k_buf=sqrt(EV%k2_buf) 
		EV%aux_buf=sqrt(1._dl+3*CP%curv/EV%k2_buf)  
		do l=1,EV%lmaxt
		EV%Kft(l)=1._dl-CP%curv*((l+1)**2-3)/EV%k2_buf
		end do
	endif


	k=EV%k_buf
	k2=EV%k2_buf

	if (CP%closed) then
		EV%FirstZerolForBeta = nint(EV%q*CP%r) 
	else 
		EV%FirstZerolForBeta=l0max !a large number
	end if

	a=tau*adotrad

	if (DoTensorNeutrinos) then
		bigR = (grhormass+grhornomass)/(grhormass+grhornomass+grhog)
	else
		bigR = 0._dl
	end if

	yt(1)=a

	yt(2)= 1._dl

	elec=-(1+2*CP%curv/k2)*(2*bigR+10)/(4*bigR+15) !elec, with H=1

	x=k*tau

	!shear
	yt(3)=-5._dl/2/(bigR+5)*x*elec 

	yt(4:EV%nvart)=0._dl

	!  Neutrinos 
	if (DoTensorNeutrinos) then
		pir=-2._dl/3._dl/(bigR+5)*x**2*elec
		aj3r=  -2._dl/21._dl/(bigR+5)*x**3*elec
		yt((EV%lmaxt-1)+(EV%lmaxt-1)*2+3+1)=pir
		yt((EV%lmaxt-1)+(EV%lmaxt-1)*2+3+2)=aj3r
	end if


end subroutine initialt


subroutine outtransf(EV, y, Arr)
	!write out clxc, clxb, clxg, clxn
	use Transfer
	implicit none
	type(EvolutionVars) EV

	real(dl) clxc, clxb, clxg, clxr, clxnu, clxtot,k,k2
	real Arr(Transfer_max)
	real(dl) y(EV%nvar)
	real(dl) qnu, a, rhonu, pnu,phi,phidot,clv,betanu     !Antz added phi,phidot

	a    = y(1)
	clxc = y(3)
	clxb = y(4)
	clxg = y(6)
	clxr = y(7+EV%lmaxg)
	k    = EV%k_buf
	k2   = EV%k2_buf

	call Quint_ValsAta(a,phi,phidot) 
	if (CP%Num_Nu_Massive > 0) then
		call Nu_background(a*avarmnu(phi,0),rhonu,pnu)

		if (EV%MassiveNuApprox) then    
			clxnu=y(EV%iq0)
		else
			betanu=avarmnu(phi,1)/avarmnu(phi,0)
			clv=y(EV%w_ix)
			!Integrate over q
			call Nu_Integrate01(a*avarmnu(phi,0),betanu,rhonu,pnu,clv,clxnu,qnu,y(EV%iq0),y(EV%iq1))
			clxnu=clxnu/rhonu
		endif

	else
		clxnu = 0
		rhonu  = 0
	end if

	clxtot = (clxc*grhoc/a + clxb*grhob/a + clxnu*grhormass/a**2*rhonu)
	clxtot=clxtot/(grhoc/a+grhob/a + grhormass/a**2*rhonu)

	Arr(Transfer_kh) = k/(CP%h0/100._dl)
	Arr(Transfer_cdm) = clxc/k2
	Arr(Transfer_b) = clxb/k2
	Arr(Transfer_g) = clxg/k2
	Arr(Transfer_r) = clxr/k2
	Arr(Transfer_nu) = clxnu/k2
	Arr(Transfer_tot) = clxtot/k2

end subroutine outtransf

!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc


subroutine fderivs(EV,n,tau,ay,ayprime)
	!  Evaluate the time derivatives of the perturbations, flat case
	!  ayprime is not neccessarily GaugeInterface.yprime, so keep them distinct
	use ThermoData
	use MassiveNu
	implicit none      
	type(EvolutionVars) EV

	integer n
	real(dl) ay(n),ayprime(n),dum2(n)  !Antz
	real(dl) tau,ep,w
	real(dl) k,k2 

	!  Internal variables.

	real(dl) opacity

	real(dl) photbar,cs2,pb43,grho,slip,clxgdot,grhormass_t, &
		clxcdot,clxbdot,drag,adotdota,gpres,clxrdot,etak
	real(dl) q,aq,v,akv(nqmax0),aqtable(nqmax0)
	real(dl) G11_t,G30_t

	real(dl) dgq,grhob_t,grhor_t,grhoc_t,grhog_t,grhov_t,grhonu_t,gpnu_t,sigma,polter
	real(dl) qgdot,qrdot,pigdot,pirdot,vbdot,dgrho,adotoa
	real(dl) a,a2,z,clxc,clxb,vb,clxg,qg,pig,clxr,qr,pir
	real(dl) rhonu,pnu,clxnu,qnu,f, dopacity
	real(dl) phi,phidot, clv, clvdot, grhoex_t
	real(dl) dum1,dpnuant,rhoconvert,tempa,tempb,betatemp,hope    !Antz
	real(dl), parameter::Mpcant=3.085678e22_dl, cant=2.99792458e8_dl,eightpi=25.13274123_dl,Gant=6.6726e-11_dl
	integer l,i,ind
	logical tcp

	real(dl) :: etadot_tmp, hdot_tmp, alpha_tmp, rhodotorho
	character (LEN=100) :: a_str, delta_m_name, v_m_name, delta_nu_name, v_nu_name
	character (LEN=100) :: stat_tmp, pos_tmp
	integer :: delta_m_file=21, v_m_file=22, delta_nu_file=23, v_nu_file=24

	k=EV%k_buf
	k2=EV%k2_buf     


	a=ay(1)
	etak=ay(2)


	!  CDM variables
	clxc=ay(3)

	!  Baryon variables
	clxb=ay(4)
	vb=ay(5)

	!  Photons
	clxg=ay(6)
	qg=ay(7)
	pig=ay(8)

	!  Massless neutrinos
	clxr=ay(7+EV%lmaxg)
	qr=ay(8+EV%lmaxg)
	pir=ay(9+EV%lmaxg)

	a2=a*a

	!  Get sound speed and ionisation fraction.
	if (EV%TightCoupling) then

		call thermo(tau,cs2,opacity,dopacity)
	else

		call thermo(tau,cs2,opacity)        

	end if

	!  Compute expansion rate from: grho 8*pi*rho*a**2

	grhob_t=grhob/a
	grhoc_t=grhoc/a
	grhor_t=grhornomass/a2
	grhog_t=grhog/a2
	grhov_t=grhov*a2
	grhormass_t=grhormass/a2
	call Quint_ValsAta(a,phi,phidot)    !Antz moved from earlier

	if (CP%Num_Nu_Massive == 0) then
		rhonu=1
		pnu=1._dl/3._dl
		clxnu=0
		qnu = 0
	else 

		!Get density and pressure as ratio to massles by interpolation from table
		call Nu_background(a*avarmnu(phi,0),rhonu,pnu)
		if (CP%MassiveNuMethod == Nu_approx) then
			clxnu=ay(EV%iq0)
			qnu=ay(EV%iq0+1)
		else
			if (EV%MassiveNuApprox) then    
				clxnu=ay(EV%iq0)
				qnu=ay(EV%iq0+2)
			else
				!Integrate over q
				betatemp=avarmnu(phi,1)/avarmnu(phi,0)
				call Nu_Integrate(a*avarmnu(phi,0),betatemp,rhonu,pnu,ay(EV%w_ix),clxnu,qnu,dpnuant,dum1,ay(EV%iq0),ay(EV%iq1),dum2)
				!clxnu_here  = rhonu*clxnu, qnu_here = qnu*rhonu
				qnu=qnu/rhonu
				clxnu = clxnu/rhonu
			endif 
		end if

	end if


	grhonu_t=grhormass_t*rhonu
	gpnu_t=grhormass_t*pnu

	!Antz
	!        write(*,*)'FISHFISH*****************'
	!        write(*,*)'rho from rhonu ',rhonu*5.68219698_dl/a**4*3._dl
	!        hope=grhonu_t/(eightpi*Gant*a**2)
	!        write(*,*)'rho from adotoa: ',hope
	!       write(*,*)'ratio: ',(rhonu*5.68219698_dl/a**4*3._dl)/(hope)
	!       write(*,*)'grho from rhonu: ',rhonu*5.68219698_dl*3._dl/a**4*Gant*eightpi*a**2
	!       write(*,*)'grho from adotoa: ',grhonu_t
	!       write(*,*)'ratio: ',rhonu*5.68219698_dl*3._dl/a**4*Gant*eightpi*a**2/grhonu_t
	!       read(*,*)
	!
	!Antz
	grho=grhob_t+grhoc_t+grhor_t+grhog_t+grhonu_t
	gpres=(grhog_t+grhor_t)/3._dl +gpnu_t


	if (has_extra) then

		!         call Quint_ValsAta(a,phi,phidot)        Antz moved to earlier
		grhoex_t=phidot**2/2 + a2*Vofphi(phi,0)
		grho=grho+grhoex_t
		gpres=gpres-grhoex_t+phidot**2     
		!  write(*,'(4E15.5)')  a, tau, grhoex_t/grho, (-grhoex_t+phidot**2)/grhoex_t

	else
		grho=grho +grhov_t
		gpres=gpres - grhov_t
	end if

	adotoa=sqrt(grho/3)
	ayprime(1)=adotoa*a

	!  Photon mass density over baryon mass density
	photbar=grhog_t/grhob_t
	pb43=4._dl/3._dl*photbar

	!  Evaluate the time derivative of sz (gradient of expansion)
	!  8*pi*a*a*SUM[rho_i*clx_i]
	dgrho=grhob_t*clxb+grhoc_t*clxc + grhog_t*clxg+grhor_t*clxr +grhonu_t*clxnu

	!  8*pi*a*a*SUM[(rho_i+p_i)*v_i]
	dgq=grhob_t*vb+grhog_t*qg+grhor_t*qr + grhonu_t*qnu

	if (has_extra) then
		clv=ay(EV%w_ix)
		clvdot=ay(EV%w_ix+1) 
		dgrho=dgrho + phidot*clvdot +clv*a2*Vofphi(phi,1)
		dgq=dgq + k*phidot*clv
	end if

	!!!     if (k>1e-5) then
	!tau a w Om_Q delta_Q
	!    write(*,'(6E15.5)') tau,a,(-grhoex_t+phidot**2)/grhoex_t,grhoex_t/grho, &
	!         (phidot*clvdot +clv*a2*Vofphi(phi,1))/grhoex_t,clxc  
	! end if


	!  Get sigma (shear) and z from the constraints
	! have to get z from eta for numerical stability
	z=(0.5_dl*dgrho/k + etak)/adotoa 
	sigma=z+1.5_dl*dgq/k2
	!  ddota/a

	adotdota=0.5_dl*(adotoa*adotoa-gpres)

	if (has_extra) then    
		rhoconvert=grhormass_t/(a**2)     !*1.677004778e-9_dl)  not needed for beta =1 units.
		tempb=avarmnu(phi,1)/avarmnu(phi,0)
		tempa=(tempb*(clxnu*rhonu-3*dpnuant))     !*4.09512488e-5_dl)   not needed for beta=1 units
		!           write(*,*)'1: ',tempa
		tempa=tempa+(avarmnu(phi,2)/avarmnu(phi,0)-tempb*tempb)*(rhonu-3*pnu)*clv


		!           write(*,*)'2: ',tempa
		!           tempa=tempa+tempb*(rhonu-3*pnu)*clvdot
		!           write(*,*)'3: ',tempa
		tempa=tempa*a2*rhoconvert
		ayprime(EV%w_ix)=   clvdot
		ayprime(EV%w_ix+1) = - 2*adotoa*clvdot - k*z*phidot - k2*clv - clv*Vofphi(phi,2)*a2- tempa
		!          ayprime(EV%w_ix)=   0._dl
		!           ayprime(EV%w_ix+1) = 0._dl

		!           write(*,*)ayprime(EV%w_ix+1)+tempa
		!           write(*,*)tempa,'             ratio   ',ayprime(EV%w_ix+1)/(ayprime(EV%w_ix+1)+tempa)
		!           write(*,*)'*************************************'
		!           write(*,*)'a: ',a,'  z:',1/a-1
		!           write(*,*)'rho: ',rhonu,'   :   pnu   ',pnu

		!           write(*,*)'p/rho: ',pnu/rhonu,'  dpnu/drhonu: ',dpnuant/(clxnu*rhonu)
		!           read(*,*)
	end if

	!eta*k equation
	ayprime(2)=0.5_dl*dgq

	!  CDM equation of motion
	clxcdot=-k*z
	ayprime(3)=clxcdot

	!  Baryon equation of motion.
	clxbdot=-k*(z+vb)
	ayprime(4)=clxbdot
	!  Photon equation of motion
	clxgdot=-k*(4._dl/3._dl*z+qg)

	!  Drag: a*n_e*sigma_T*(4/3*vb-qg)
	drag=opacity*(4._dl/3._dl*vb-qg)

	! Matter perturbations for GNQ simulation

	if (CP%write_matter_perturbations .and. (a > CP%gnqsim_a_init) &
			.and. .not. k_plotted_matter) then

		! Write matter perturbations

		! Create filenames from root
		write (a_str, '(f10.2)') CP%gnqsim_a_init
		a_str = adjustl(a_str)
		delta_m_name = 'output/pert' // trim(a_str) // '_' &
				// trim(CP%root_filename) // '_delta_m.dat'
		v_m_name = 'output/pert' // trim(a_str) // '_' &
				// trim(CP%root_filename) // '_v_m.dat'

		! Calculate necessary quantities (gauge transformation from synchronous
		! to Newtonian gauge); cf. Ma & Bertschinger for notations
		etadot_tmp = ayprime(2) / k
		hdot_tmp = 2.d0 * k * z
		alpha_tmp = (hdot_tmp + 6.d0 * etadot_tmp) / (2.d0 * k**2)
		rhodotorho = -3.d0 * adotoa

		! Create new file or append?
		if (k_started_matter) then
			stat_tmp = 'old'
			pos_tmp = 'append'
		else
			stat_tmp = 'replace'
			pos_tmp = 'rewind'
		end if

		! Open files and write

		open (delta_m_file, file = trim(delta_m_name), position = trim(pos_tmp), &
				status = trim(stat_tmp))
		open (v_m_file, file = trim(v_m_name), position = trim(pos_tmp), &
				status = trim(stat_tmp))

		write (delta_m_file, *) k, clxc + alpha_tmp * rhodotorho
		write (v_m_file, *) k, alpha_tmp * k

		close (delta_m_file)
		close (v_m_file)

		k_plotted_matter = .true.
		k_started_matter = .true.
	end if

	if (a < CP%gnqsim_a_init / 2.d0) k_plotted_matter = .false.

	! Neutrino perturbations for GNQ simulation

	if (CP%write_nu_perturbations .and. (a > CP%gnqsim_a_add_nu) &
			.and. .not. k_plotted_nu) then

		! Write matter perturbations

		! Create filenames from root
		write (a_str, '(f10.2)') CP%gnqsim_a_add_nu
		a_str = adjustl(a_str)
		delta_nu_name = 'output/pert' // trim(a_str) // '_' &
				// trim(CP%root_filename) // '_delta_nu.dat'
		v_nu_name = 'output/pert' // trim(a_str) // '_' &
				// trim(CP%root_filename) // '_v_nu.dat'

		! Calculate necessary quantities (gauge transformation from synchronous
		! to Newtonian gauge); cf. Ma & Bertschinger for explanations
		etadot_tmp = ayprime(2) / k
		hdot_tmp = 2.d0 * k * z
		alpha_tmp = (hdot_tmp + 6.d0 * etadot_tmp) / (2.d0 * k**2)
		rhodotorho = -3.d0 * adotoa * (1.d0 + pnu/rhonu) &
				- (-1.d0 * betaofphi(phi)) * phidot * (1.d0 - 3.d0 * pnu/rhonu)

		! Create new file or append?
		if (k_started_nu) then
			stat_tmp = 'old'
			pos_tmp = 'append'
		else
			stat_tmp = 'replace'
			pos_tmp = 'rewind'
		end if

		! Open files and write

		open (delta_nu_file, file = trim(delta_nu_name), position = trim(pos_tmp), &
				status = trim(stat_tmp))
		open (v_nu_file, file = trim(v_nu_name), position = trim(pos_tmp), &
				status = trim(stat_tmp))

		write (delta_nu_file, *) k, clxnu + alpha_tmp * rhodotorho
		write (v_nu_file, *) k, qnu + alpha_tmp * k

		close (delta_nu_file)
		close (v_nu_file)

		k_plotted_nu = .true.
		k_started_nu = .true.
	end if

	if (a < CP%gnqsim_a_add_nu / 2.d0) k_plotted_nu = .false.

	! end GNQ simulation outputs

	!  use tight coupling approx?
	if (k > epsw) then
		ep=ep0
	else
		ep=0.5_dl*ep0
	end if
	!     There was an instability for low k when we 
	!     switched off tight coupling. Minor change in 
	!     criteria ends the problem. (Thanks to David Spergel)      
	tcp = ((k/opacity > ep).or.(1._dl/(opacity*tau) > ep .and. k/opacity > 1d-4)) 

	!  Use explicit equation for vb if appropriate

	if (EV%TightCoupling) then

		!  Use tight-coupling approximation for vb
		!  zeroth order (in t_c) approximation to vbdot
		vbdot=(-adotoa*vb+cs2*k*clxb  &
			+0.25_dl*k*pb43*(clxg-2._dl*pig))/(1._dl+pb43)

		slip = - (2*adotoa/(1+pb43) + dopacity/opacity)*(vb-3._dl/4*qg) &
			+(-adotdota*vb-k/2*adotoa*clxg +k*(cs2*clxbdot-clxgdot/4))/(opacity*(1+pb43))

		!  First-order approximation to vbdot
		vbdot=vbdot+pb43/(1._dl+pb43)*slip

	else
		vbdot=-adotoa*vb+cs2*k*clxb-photbar*opacity*(4._dl/3*vb-qg)
	end if    

	ayprime(5)=vbdot

	!  Photon equations of motion
	ayprime(6)=clxgdot
	qgdot=4._dl/3._dl*(-vbdot-adotoa*vb+cs2*k*clxb)/pb43 &
		+k*(clxg-2._dl*pig)/3._dl
	ayprime(7)=qgdot

	polter = 0.1_dl*pig+9._dl/15._dl*ay(EV%polind+2) !2/15*(3/4 pig + 9/2 E2)
	!  Use explicit equations for photon moments if appropriate       
	if (EV%tightcoupling) then

		!  Use tight-coupling approximation where moments are zero for l>1
		pigdot=0._dl
		ayprime(8:EV%lmaxg+6)=0._dl

	else
		pigdot=0.4_dl*k*qg-0.6_dl*k*ay(9)-opacity*(pig - polter) &
			+8._dl/15._dl*k*sigma
		ayprime(8)=pigdot
		do  l=3,EV%lmaxg-1
		ayprime(l+6)=k*denl(l)*(l*ay(l+5)-(l+1)*ay(l+7))-opacity*ay(l+6)
		end do
		!  Truncate the photon moment expansion
		ayprime(EV%lmaxg+6)=k*ay(EV%lmaxg+5)-(EV%lmaxg+1)/tau*ay(EV%lmaxg+6)  &
			-opacity*ay(EV%lmaxg+6)

	end if
	!  Massless neutrino equations of motion.
	clxrdot=-k*(4._dl/3._dl*z+qr)
	ayprime(EV%lmaxg+7)=clxrdot
	qrdot=k*(clxr-2._dl*pir)/3._dl
	ayprime(EV%lmaxg+8)=qrdot
	pirdot=k*(0.4_dl*qr-0.6_dl*ay(EV%lmaxg+10)+8._dl/15._dl*sigma)
	ayprime(EV%lmaxg+9)=pirdot
	do l=3,EV%lmaxnr-1
	ayprime(l+EV%lmaxg+7)=k*denl(l)*(l*ay(l+EV%lmaxg+6) -(l+1)*ay(l+EV%lmaxg+8))
	end do   
	!  Truncate the neutrino expansion
	ayprime(EV%lmaxnr+EV%lmaxg+7)=k*ay(EV%lmaxnr+EV%lmaxg+6)- &
		(EV%lmaxnr+1)/tau*ay(EV%lmaxnr+EV%lmaxg+7)

	!  Polarization
	if (EV%TightCoupling) then     

		ayprime(EV%polind+2:EV%polind+EV%lmaxgpol)=0._dl

	else
		!l=2 (defined to be zero for l<2)
		ayprime(EV%polind+2) = -opacity*(ay(EV%polind+2) - polter) - k/3._dl*ay(EV%polind+3)
		!and the rest
		do l=3,EV%lmaxgpol-1
		ayprime(EV%polind+l)=-opacity*ay(EV%polind+l) + k*denl(l)*(l*ay(EV%polind+l-1) -&
			polfac(l)*ay(EV%polind+l+1))
		end do  

		!truncate
		ayprime(EV%polind+EV%lmaxgpol)=-opacity*ay(EV%polind+EV%lmaxgpol) + &
			k*EV%poltruncfac*ay(EV%polind+EV%lmaxgpol-1)-(EV%lmaxgpol+3)*ay(EV%polind+EV%lmaxgpol)/tau

	end if

	!  Massive neutrino equations of motion.
	if (CP%Num_Nu_massive == 0) return

	if (CP%MassiveNuMethod == Nu_approx) then

		!crude approximation scheme
		w=pnu/rhonu

		f = (5._dl/3)**(a*avarmnu(phi,0)/(a*avarmnu(phi,0)+200))*(3*w)**((2+a*avarmnu(phi,0))/(4+a*avarmnu(phi,0))) 

		!clxnudot
		ayprime(EV%iq0)=-(f-3*w)*adotoa*clxnu-k*((1+w)*z+qnu)
		!qnudot
		ayprime(EV%iq0+1)=-adotoa*(1-3*w)*ay(EV%iq0+1)+k/3._dl*(f*clxnu-2._dl*ay(EV%iq0+2))
		!pinudot

		ayprime(EV%iq0+2)=-adotoa*(-f +2-3*w)*ay(EV%iq0+2) +  &
			k*(2._dl/5*f*qnu-0.6_dl*ay(EV%iq0+3)+ 2._dl/5*w*(5-3*w)*sigma)

		do l=3,EV%lmaxnu-1
		ayprime(EV%iq0+l)=-adotoa*((1-l)*f+l-3*w)*ay(EV%iq0+l) + &
			k*denl(l)*(f*l*ay(EV%iq0+l-1) -(l+1)*ay(EV%iq0+l+1))
		end do
		!  Truncate the neutrino expansion
		ayprime(EV%iq0+EV%lmaxnu)=k*ay(EV%iq0+EV%lmaxnu-1)- &
			(EV%lmaxnu+1)/tau*ay(EV%iq0+EV%lmaxnu)


	else !Not approx scheme

		if (EV%MassiveNuApprox) then

			!Now EV%iq0 = clx, EV%iq0+1 = clxp, EV%iq0+2 = G_1, EV%iq0+3=G_2=pinu
			G11_t=EV%G11/a/a2 
			G30_t=EV%G30/a/a2
			w=pnu/rhonu
			ayprime(EV%iq0)=-k*z*(w+1) + 3*adotoa*(w*ay(EV%iq0) - ay(EV%iq0+1))-k*ay(EV%iq0+2)
			ayprime(EV%iq0+1)=(3*w-2)*adotoa*ay(EV%iq0+1) - 5._dl/3*k*z*w - k/3*G11_t
			ayprime(EV%iq0+2)=(3*w-1)*adotoa*ay(EV%iq0+2) - k*(2._dl/3*ay(EV%iq0+3)-ay(EV%iq0+1))
			ayprime(EV%iq0+3)=(3*w-2)*adotoa*ay(EV%iq0+3) + 2*w*k*sigma - k/5*(3*G30_t-2*G11_t)

		else

			do i=1,nqmax
			q=i*dq-0.5_dl
			aq=a*avarmnu(phi,0)/q
			aqtable(i)=aq
			v=1._dl/sqrt(1._dl+aq*aq)
			akv(i)=k*v        
			end do

			!  l = 0, 1, 2,EV%lmaxnu.
			do i=1,nqmax
			ind=EV%iq0+i-1
			ayprime(ind)=-akv(i)*ay(ind+nqmax)+z*k*dlfdlq(i)/3
			ind=EV%iq1+i-1
			ayprime(ind)=akv(i)*(ay(ind-nqmax)-2*ay(ind+nqmax))/3  &
				!ANTZ correction term!!!
			-1._dl/3._dl*dlfdlq(i)*ay(EV%w_ix)*tempb*akv(i)*aqtable(i)**2
			ind=EV%iq2+i-1

			ayprime(ind)=akv(i)*(2*ay(ind-nqmax)-3*ay(ind+nqmax))/5 &
				-k*2._dl/15._dl*sigma*dlfdlq(i)
			ind=EV%iq0+i-1+EV%lmaxnu*nqmax
			!  Truncate moment expansion.
			ayprime(ind)=akv(i)*ay(ind-nqmax)-(EV%lmaxnu+1)/tau*ay(ind)
			end do
			do l=3,EV%lmaxnu-1
			do i=1,nqmax
			ind=EV%iq0-1+i+l*nqmax
			ayprime(ind)=akv(i)*denl(l)*(l*ay(ind-nqmax)-(l+1)*ay(ind+nqmax))
			end do
			end do

		end if

	end if

end subroutine fderivs


subroutine fderivsv(EV,n,tau,yv,yvprime)
	!  Evaluate the time derivatives of the vector perturbations, flat case
	implicit none      
	type(EvolutionVars) EV
	integer n
	real(dl), target ::  yv(n),yvprime(n)
	real(dl) tau

	stop 'vectors not implemented in this module'

end subroutine fderivsv

subroutine initialv(EV,yv,tau)
	!  Initial conditions for vectors

	implicit none
	type(EvolutionVars) EV
	real(dl) yv(EV%nvarv)
	real(dl) tau

	stop 'vectors not implemented in this module'

end  subroutine initialv      


subroutine outputv(EV,yv,n,j,tau,dt,dte,dtb)
	!calculate the vector sources 
	use ThermoData

	implicit none
	integer n,j
	type(EvolutionVars) :: EV
	real(dl), target :: yv(n), yvprime(n)
	real(dl) tau,dt,dte,dtb

	stop 'vectors not implemented in this module'

end subroutine outputv


!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
subroutine fderivst(EV,n,tau,ayt,aytprime)
	!  Evaluate the time derivatives of the tensor perturbations, flat case
	use ThermoData
	use MassiveNu
	implicit none      
	type(EvolutionVars) EV
	integer n,l,i,ind
	real(dl), target ::  ayt(n),aytprime(n)
	real(dl) ep,tau,grho,rhopi,cs2,opacity,gpres,pirdt,grhormass_t
	logical tcp
	real(dl), dimension(:),pointer :: neut,neutprime,E,B,Eprime,Bprime
	real(dl) q,aq,v,akv(nqmax0),shearnu
	real(dl)  grhob_t,grhor_t,grhoc_t,grhog_t,grhov_t,grhonu_t,gpnu_t,polter
	real(dl) Hchi,shear, pig
	real(dl) k,k2,a,a2
	real(dl) pinu,pir,pnu,adotoa, rhonu
	real(dl) phi,phidot, grhoex_t

	k2=EV%k2_buf
	k=EV%k_buf       
	stop 'code not fixed for tensors mavans'
	!E and B start at l=2. Set up pointers accordingly to fill in y arrays
	E => ayt(EV%lmaxt+3-1:)
	Eprime=> aytprime(EV%lmaxt+3-1:) 
	B => E(EV%lmaxpolt:)
	Bprime => Eprime(EV%lmaxpolt:)



	a=ayt(1)        

	!Hchi = metric perturbation variable, conserved on large scales
	Hchi=ayt(2)

	!shear (Hchi' = - k*shear)
	shear=ayt(3)

	!  Photon anisotropic stress
	pig=ayt(4)


	a2=a*a
	call Quint_ValsAta(a,phi,phidot)
	!  Get sound speed and opacity, and see if should use tight-coupling

	call thermo(tau,cs2,opacity)
	if (k > 0.06_dl*epsw) then
		ep=ep0
	else
		ep=0.2_dl*ep0
	end if

	tcp = ((k/opacity > ep).or.(1._dl/(opacity*tau) > ep)) 

	!Do massive neutrinos
	if (CP%Num_Nu_Massive ==0) then
		rhonu=1._dl
		pnu=1._dl/3._dl
	else
		call Nu_background(a*avarmnu(phi,0),rhonu,pnu)

	end if

	! Compute expansion rate from: grho=8*pi*rho*a**2
	! Also calculate gpres: 8*pi*p*a**2
	grhob_t=grhob/a
	grhoc_t=grhoc/a
	grhor_t=grhornomass/a2
	grhog_t=grhog/a2
	grhov_t=grhov*a2
	grhormass_t=grhormass/a2

	grhonu_t=grhormass_t*rhonu
	gpnu_t=grhormass_t*pnu

	grho=grhob_t+grhoc_t+grhor_t+grhog_t+grhonu_t

	gpres=(grhog_t+grhor_t)/3._dl+gpnu_t

	if (has_extra) then

		!        call Quint_ValsAta(a,phi,phidot)                 Antz  moved to earlier
		grhoex_t=phidot**2/2 + a2*Vofphi(phi,0)
		grho=grho+grhoex_t
		gpres=gpres-grhoex_t+phidot**2     
	else
		grho=grho +grhov_t
		gpres=gpres - grhov_t
	end if

	adotoa=sqrt(grho/3._dl)

	aytprime(1)=adotoa*a
	polter = 0.1_dl*pig + 9._dl/15._dl*E(2)



	if (tcp) then
		!  Use explicit equations:
		!  Equation for the photon anisotropic stress
		aytprime(4)=k*(-1._dl/3._dl*ayt(5)+8._dl/15._dl*shear)  &
			-opacity*(pig - polter)
		! And for the moments            
		do  l=3,EV%lmaxt-1
		aytprime(l+2)=k*denl(l)*(l*ayt(l+1)-   &
			tensfac(l)*ayt(l+3))-opacity*ayt(l+2)
		end do
		!  Truncate the hierarchy

		aytprime(EV%lmaxt+2)=k*EV%lmaxt/(EV%lmaxt-2._dl)*ayt(EV%lmaxt+1)- &
			(EV%lmaxt+3._dl)*ayt(EV%lmaxt+2)/tau-opacity*ayt(EV%lmaxt+2)

		!E equations

		Eprime(2) = - opacity*(E(2) - polter) + k*(4._dl/6._dl*B(2) - &
			5._dl/27._dl*E(3))
		do l=3,EV%lmaxpolt-1
		Eprime(l) =-opacity*E(l) + k*(denl(l)*(l*E(l-1) - &
			tensfacpol(l)*E(l+1)) + 4._dl/(l*(l+1))*B(l))
		end do
		!truncate
		Eprime(EV%lmaxpolt)=0._dl


		!B-bar equations

		do l=2,EV%lmaxpolt-1
		Bprime(l) =-opacity*B(l) + k*(denl(l)*(l*B(l-1) - &
			tensfacpol(l)*B(l+1)) - 4._dl/(l*(l+1))*E(l))
		end do
		!truncate
		Bprime(EV%lmaxpolt)=0._dl

	else

		ayt(4)=32._dl/45._dl*k/opacity*shear   
		E(2)=ayt(4)/4._dl
		!  Set the derivatives to zero
		aytprime(4:n)=0._dl

	endif


	!  Neutrino equations: 
	!  Anisotropic stress  
	if (DoTensorNeutrinos) then

		neutprime => Bprime(EV%lmaxpolt:)
		neut => B(EV%lmaxpolt:)

		!  Massless neutrino anisotropic stress
		pir=neut(2)

		pirdt=-1._dl/3._dl*k*neut(3)+ 8._dl/15._dl*k*shear
		neutprime(2)=pirdt
		!  And for the moments
		do  l=3,EV%lmaxnrt-1
		neutprime(l)=k*denl(l)*(l*neut(l-1)- tensfac(l)*neut(l+1))
		end do

		!  Truncate the hierarchy
		neutprime(EV%lmaxnrt)=k*EV%lmaxnrt/(EV%lmaxnrt-2._dl)*neut(EV%lmaxnrt-1)-  &
			(EV%lmaxnrt+3._dl)*neut(EV%lmaxnrt)/tau

		!  Massive neutrino equations of motion.
		if (CP%Num_Nu_massive /= 0) then

			do i=1,nqmax
			q=i*dq-0.5_dl
			aq=a*avarmnu(phi,0)/q
			v=1._dl/sqrt(1._dl+aq*aq)
			akv(i)=k*v
			end do
			do i=1,nqmax
			ind=EV%iqt+i-1                          
			aytprime(ind)=-1._dl/3._dl*akv(i)*ayt(ind+nqmax)- 2._dl/15._dl*k*shear*dlfdlq(i)
			ind=EV%iqt+i-1+(EV%lmaxnut-2)*nqmax
			!  Truncate moment expansion.
			aytprime(ind)=akv(i)*EV%lmaxnut/(EV%lmaxnut-2._dl)*ayt(ind-nqmax)-(EV%lmaxnut+3)/tau*ayt(ind)
			end do
			do l=3,EV%lmaxnut-1
			do i=1,nqmax
			ind=EV%iqt-1+i+(l-2)*nqmax
			aytprime(ind)=akv(i)*denl(l)*(l*ayt(ind-nqmax)-tensfac(l)*ayt(ind+nqmax))     
			end do
			end do

			call Nu_Shear(a*avarmnu(phi,0),shearnu,ayt(EV%iqt))                 
			pinu=1.5_dl*shearnu/rhonu   

		else
			pinu=0._dl
		end if

	else
		pinu=0
		pir=0
		pirdt=0
	end if

	!  Get the propagation equation for the shear


	rhopi=grhog_t*pig+grhor_t*pir+grhonu_t*pinu

	aytprime(3)=-2*adotoa*shear+k*Hchi-rhopi/k   

	if (tcp) then
		!  Use the full expression for pigdt
		EV%tenspigdot=aytprime(4)
	else
		!  Use the tight-coupling approximation
		EV%tenspigdot= 32._dl/45._dl*k/opacity*(2._dl*adotoa*shear+aytprime(3))
	endif

	aytprime(2)=-k*shear


end subroutine fderivst


!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
subroutine derivs(EV,n,tau,ay,ayprime)
	!  Evaluate the time derivatives of the perturbations.
	!ayprime is not neccessarily GaugeInterface.yprime, so keep them distinct
	use ThermoData
	use MassiveNu
	implicit none      
	type(EvolutionVars) EV
	integer n
	real(dl) ay(n),ayprime(n)
	real(dl) tau

	real(dl) dgq,grhob_t,grhor_t,grhoc_t,grhog_t,grhov_t,grhonu_t,gpnu_t,sigma,polter
	real(dl) qgdot,qrdot,pigdot,pirdot,vbdot,dgrho,adotoa
	real(dl) a,a2,z,clxc,clxb,vb,clxg,qg,pig,clxr,qr,pir,rhonu,pnu,clxnu,qnu
	real(dl) k,k2 
	!  Internal variables.

	real(dl) opacity,ep,cothxor,w

	real(dl) photbar,cs2,pb43,grho,slip,clxgdot,etak, &
		clxcdot,clxbdot,drag,adotdota,gpres,clxrdot
	real(dl) q,aq,v,akv(nqmax0),grhormass_t
	integer l,i,ind
	logical tcp !false if can use tight coupling approx

	real(dl) G11_t,G30_t, f
	real(dl) clv,clvdot, phidot, phi,grhoex_t
	real(dl) betanu                                   !Antz


	k2=EV%k2_buf
	k=EV%k_buf


	cothxor=1._dl/tanfunc(tau/CP%r)/CP%r      

	a=ay(1)
	!z=ay(2)/a
	etak=ay(2)

	!  CDM variables
	clxc=ay(3)

	!  Baryon variables
	clxb=ay(4)
	vb=ay(5)

	!  Photons
	clxg=ay(6)
	qg=ay(7)
	pig=ay(8)

	!  Massless neutrinos
	clxr=ay(7+EV%lmaxg)
	qr=ay(8+EV%lmaxg)
	pir=ay(9+EV%lmaxg)

	a2=a*a

	!   Antz from later....

	clv=ay(EV%w_ix)
	clvdot=ay(EV%w_ix+1)

	!  Get baryon temperature, sound speed and ionisation fraction.
	call thermo(tau,cs2,opacity)

	!  Compute expansion rate from: grho 8*pi*rho*a**2

	grhob_t=grhob/a
	grhoc_t=grhoc/a
	grhor_t=grhornomass/a2
	grhog_t=grhog/a2
	grhov_t=grhov*a2
	grhormass_t=grhormass/a2
	call Quint_ValsAta(a,phi,phidot)                  !Antz moved from earlier
	if (CP%Num_Nu_Massive == 0) then
		rhonu=1._dl
		pnu=1._dl/3._dl
		clxnu=0._dl
		qnu = 0
	else
		!Get density and pressure as ratio to massles by interpolation from table
		call Nu_background(a*avarmnu(phi,0),rhonu,pnu)
		if (CP%MassiveNuMethod == Nu_approx) then

			clxnu=ay(EV%iq0)
			qnu=ay(EV%iq0+1)

		else
			if (EV%MassiveNuApprox) then    
				clxnu=ay(EV%iq0)
				qnu=ay(EV%iq0+2)
			else
				!Integrate over q
				betanu=avarmnu(phi,1)/avarmnu(phi,0)
				call Nu_Integrate01(a*avarmnu(phi,0),betanu,rhonu,pnu,clv,clxnu,qnu,ay(EV%iq0),ay(EV%iq1))
				!clxnu_here  = rhonu*clxnu, qnu_here = qnu*rhonu
				qnu=qnu/rhonu
				clxnu = clxnu/rhonu
			endif 
		end if
	end if

	grhonu_t=grhormass_t*rhonu
	gpnu_t=grhormass_t*pnu


	grho=grhob_t+grhoc_t+grhor_t+grhog_t+grhonu_t
	gpres=(grhog_t+grhor_t)/3._dl +gpnu_t

	if (has_extra) then

		!         call Quint_ValsAta(a,phi,phidot)                   Antz  moved to earlier
		grhoex_t=phidot**2/2 + a2*Vofphi(phi,0)
		grho=grho+grhoex_t
		gpres=gpres-grhoex_t+phidot**2     
	else
		grho=grho +grhov_t
		gpres=gpres - grhov_t

	end if


	adotoa=sqrt((grho+grhok)/3._dl)
	ayprime(1)=adotoa*a

	!  Photon mass density over baryon mass density
	photbar=grhog_t/grhob_t
	pb43=4._dl/3._dl*photbar        


	!  8*pi*a*a*SUM[rho_i*clx_i]
	dgrho=grhob_t*clxb+grhoc_t*clxc + grhog_t*clxg+grhor_t*clxr +grhonu_t*clxnu

	!  8*pi*a*a*SUM[(rho_i+p_i)*v_i]
	dgq=grhob_t*vb+grhog_t*qg+grhor_t*qr+ grhonu_t*qnu

	if (has_extra) then
		!           clv=ay(EV%w_ix)                              Antz moved to earlier
		!           clvdot=ay(EV%w_ix+1)                         Antz moved to earlier
		dgrho=dgrho + phidot*clvdot +clv*a2*Vofphi(phi,1)
		dgq=dgq + k*phidot*clv
	end if

	!  Get sigma (shear) and z from the constraints
	!  have to get z from eta for numerical stability       
	z=(0.5_dl*dgrho/k + etak)/adotoa 
	sigma=(z+1.5_dl*dgq/k2)/EV%Kf(1)


	!  ddota/a

	adotdota=0.5_dl*(adotoa*adotoa-gpres)


	if (has_extra) then
		ayprime(EV%w_ix)= clvdot
		ayprime(EV%w_ix+1) = - 2*adotoa*clvdot - k*z*phidot - k2*clv - clv*Vofphi(phi,2)
	end if


	!  eta*k equation
	ayprime(2)=0.5_dl*dgq + CP%curv*z

	!  CDM equation of motion
	clxcdot=-k*z
	ayprime(3)=clxcdot
	!  Baryon equation of motion.
	clxbdot=-k*(z+vb)
	ayprime(4)=clxbdot
	!  Photon equation of motion
	clxgdot=-k*(4._dl/3._dl*z+qg)

	!  Drag: a*n_e*sigma_T*(4/3*vb-qg)
	drag=opacity*(4._dl/3._dl*vb-qg)

	!  use tight coupling approx?
	if (k > 0.06_dl*epsw) then
		ep=ep0
	else
		ep=1.1_dl*ep0
	end if

	!     There was an instability for low k when we 
	!     switched off tight coupling. Minor change in 
	!     criteria ends the problem. (Thanks to David Spergel)
	tcp = ((EV%q/opacity > ep).or.(1._dl/(opacity*tau) > ep .and. EV%q/opacity > 1d-4))          

	!  Use explicit equation for vb if appropriate

	if (tcp) then
		vbdot=-adotoa*vb+cs2*k*clxb-photbar*drag
	else

		!  Use tight-coupling approximation for vb
		!  zeroth order (in t_c) approximation to vbdot
		vbdot=(-adotoa*vb+cs2*k*clxb  &
			+0.25_dl*k*pb43*(clxg-2._dl*EV%Kf(1)*pig))/(1._dl+pb43) 


		!  First-order (in t_c) approximation to baryon-photon splip
		slip=2._dl*pb43/(1._dl+pb43)*adotoa*(vb-3._dl/4._dl*qg) &
			+1._dl/opacity*(-adotdota*vb-0.5_dl*k*adotoa*clxg  &
			+k*(cs2*clxbdot-0.25_dl*clxgdot))/(1._dl+pb43)

		!  First-order approximation to vbdot
		vbdot=vbdot+pb43/(1._dl+pb43)*slip
	end if
	ayprime(5)=vbdot

	!  Photon equations of motion
	ayprime(6)=clxgdot
	qgdot=4._dl/3._dl*(-vbdot-adotoa*vb+cs2*k*clxb)/pb43 &
		+k*(clxg-2._dl*pig*EV%Kf(1))/3._dl
	ayprime(7)=qgdot


	if (EV%FirstZerolForBeta <= EV%MaxlNeeded) ayprime(8:EV%ScalEqsToPropagate)=0 !bit lazy this

	!  Use explicit equations for photon moments if appropriate

	polter = 0.1_dl*pig+9._dl/15._dl*ay(EV%polind+2) !2/15*(3/4 pig + 9/2 E2)
	if (tcp) then
		pigdot=0.4_dl*k*qg-0.6_dl*k*EV%Kf(2)*ay(9)-opacity*(pig - polter) &
			+8._dl/15._dl*k*sigma
		ayprime(8)=pigdot
		do  l=3,min(EV%FirstZerolForBeta,EV%lmaxg)-1
		ayprime(l+6)=k*denl(l)*(l*ay(l+5)-(l+1)*EV%Kf(l)*ay(l+7))-opacity*ay(l+6)
		end do
		!  Truncate the photon moment expansion
		if (EV%lmaxg/=EV%FirstZerolForBeta) then

			ayprime(EV%lmaxg+6)=k*ay(EV%lmaxg+5)-(EV%lmaxg+1)*cothxor*ay(EV%lmaxg+6)  &
				-opacity*ay(EV%lmaxg+6)
		end if

	else
		!  Use tight-coupling approximation where moments are zero for l>1
		pigdot=0._dl

		ayprime(8:EV%lmaxg+6)=0._dl

	end if


	!  Massless neutrino equations of motion.
	clxrdot=-k*(4._dl/3._dl*z+qr)
	ayprime(EV%lmaxg+7)=clxrdot
	qrdot=k*(clxr-2._dl*pir*EV%Kf(1))/3._dl
	ayprime(EV%lmaxg+8)=qrdot
	pirdot=k*(0.4_dl*qr-0.6_dl*EV%Kf(2)*ay(EV%lmaxg+10)+8._dl/15._dl*sigma)
	ayprime(EV%lmaxg+9)=pirdot
	do l=3,min(EV%FirstZerolForBeta,EV%lmaxnr)-1
	ayprime(l+EV%lmaxg+7)=k*denl(l)*(l*ay(l+EV%lmaxg+6) -(l+1)*EV%Kf(l)*ay(l+EV%lmaxg+8))
	end do   
	!  Truncate the neutrino expansion  
	if (EV%lmaxnr/=EV%FirstZerolForBeta) then

		ayprime(EV%lmaxnr+EV%lmaxg+7)=k*ay(EV%lmaxnr+EV%lmaxg+6)- &
			(EV%lmaxnr+1)*cothxor*ay(EV%lmaxnr+EV%lmaxg+7)
	end if

	!  Polarization
	if (tcp) then     
		!l=2 (defined to be zero for l<2)
		ayprime(EV%polind+2) = -opacity*(ay(EV%polind+2) - polter) - k/3._dl*EV%Kf(2)*ay(EV%polind+3)
		!and the rest
		do l=3,min(EV%FirstZerolForBeta,EV%lmaxgpol)-1
		ayprime(EV%polind+l)=-opacity*ay(EV%polind+l) + k*denl(l)*(l*ay(EV%polind+l-1) -&
			polfac(l)*EV%Kf(l)*ay(EV%polind+l+1))
		end do  

		!truncate
		if (EV%lmaxgpol/=EV%FirstZerolForBeta) then   

			ayprime(EV%polind+EV%lmaxgpol)=-opacity*ay(EV%polind+EV%lmaxgpol) +  &
				k*EV%poltruncfac*ay(EV%polind+EV%lmaxgpol-1)-(EV%lmaxgpol+3)*cothxor*ay(EV%polind+EV%lmaxgpol)
		end if

	else
		ayprime(EV%polind+2:EV%polind+EV%lmaxgpol)=0._dl
	end if
	!  Massive neutrino equations of motion.
	if (CP%Num_Nu_massive == 0) return

	if (CP%MassiveNuMethod == Nu_approx) then

		!crude approximation scheme
		w=pnu/rhonu
		f = (5._dl/3)**(a*avarmnu(phi,0)/(a*avarmnu(phi,0)+200))*(3*w)**((2+a*avarmnu(phi,0))/(4+a*avarmnu(phi,0))) 

		!clxnudot
		ayprime(EV%iq0)=-(f-3*w)*adotoa*clxnu-k*((1+w)*z+qnu)
		!qnudot
		ayprime(EV%iq0+1)=-adotoa*(1-3*w)*ay(EV%iq0+1)+k/3._dl*(f*clxnu-2._dl*EV%Kf(1)*ay(EV%iq0+2))
		!pinudot

		ayprime(EV%iq0+2)=-adotoa*(-f +2-3*w)*ay(EV%iq0+2) +  &
			k*(2._dl/5*f*qnu-0.6_dl*EV%Kf(2)*ay(EV%iq0+3)+ 2._dl/5*w*(5-3*w)*sigma)

		do l=3,EV%lmaxnu-1
		ayprime(EV%iq0+l)=-adotoa*((1-l)*f+l-3*w)*ay(EV%iq0+l) + &
			k*denl(l)*(f*l*ay(EV%iq0+l-1) -EV%Kf(l)*(l+1)*ay(EV%iq0+l+1))
		end do
		!  Truncate the neutrino expansion
		ayprime(EV%iq0+EV%lmaxnu)=k*ay(EV%iq0+EV%lmaxnu-1)- &
			(EV%lmaxnu+1)*cothxor*ay(EV%iq0+EV%lmaxnu)


	else !Not approx scheme

		if (EV%MassiveNuApprox) then
			!Now EV%iq0 = clx, EV%iq0+1 = clxp, EV%iq0+2 = G_1, EV%iq0+3=G_2=pinu
			G11_t=EV%G11/a2/a
			G30_t=EV%G30/a2/a
			w=pnu/rhonu
			ayprime(EV%iq0)=-k*z*(w+1) + 3*adotoa*(w*ay(EV%iq0) - ay(EV%iq0+1))-k*ay(EV%iq0+2)
			ayprime(EV%iq0+1)=(3*w-2)*adotoa*ay(EV%iq0+1) - 5._dl/3*k*z*w - k/3*G11_t
			ayprime(EV%iq0+2)=(3*w-1)*adotoa*ay(EV%iq0+2) - k*(2._dl/3*EV%Kf(1)*ay(EV%iq0+3)-ay(EV%iq0+1))
			ayprime(EV%iq0+3)=(3*w-2)*adotoa*ay(EV%iq0+3) + 2*w*k*sigma - k/5*(3*EV%Kf(2)*G30_t-2*G11_t)

		else


			do i=1,nqmax
			q=i*dq-0.5_dl
			aq=a*avarmnu(phi,0)/q
			v=1._dl/sqrt(1._dl+aq*aq)
			akv(i)=k*v
			end do
			!  l = 0, 1, 2,EV%lmaxnu.
			do i=1,nqmax             
			ind=EV%iq0+i-1
			ayprime(ind)=-akv(i)*ay(ind+nqmax)+z*k*dlfdlq(i)/3
			ind=EV%iq1+i-1
			ayprime(ind)=akv(i)*(ay(ind-nqmax)-2*EV%Kf(1)*ay(ind+nqmax))/3
			ind=EV%iq2+i-1
			ayprime(ind)=akv(i)*(2*ay(ind-nqmax)-3*EV%Kf(2)*ay(ind+nqmax))/5 &
				-k*2._dl/15._dl*sigma*dlfdlq(i)
			ind=EV%iq0+i-1+EV%lmaxnu*nqmax
			!  Truncate moment expansion.
			if (EV%lmaxnu<EV%FirstZerolForBeta) then
				ayprime(ind)=akv(i)*ay(ind-nqmax)-(EV%lmaxnu+1)*cothxor*ay(ind)

			end if         

			end do
			do l=3,min(EV%FirstZerolForBeta,EV%lmaxnu)-1
			do i=1,nqmax
			ind=EV%iq0-1+i+l*nqmax
			ayprime(ind)=akv(i)*denl(l)*(l*ay(ind-nqmax)-(l+1)*EV%Kf(l)*ay(ind+nqmax))
			end do
			end do  


		end if     

	end if
end subroutine derivs

!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc 
subroutine derivst(EV,n,tau,ayt,aytprime)
	!  Evaluate the time derivatives of the tensor perturbations.
	use ThermoData
	use MassiveNu

	implicit none      
	type(EvolutionVars) EV
	integer n,l,i,ind
	real(dl), target ::  ayt(n),aytprime(n)
	real(dl) ep,tau,grho,rhopi,cs2,opacity,gpres,pirdt,cothxor,grhormass_t
	logical tcp
	real(dl), dimension(:),pointer :: neut,neutprime,E,B,Eprime,Bprime
	real(dl) q,aq,v,akv(nqmax0),shearnu
	real(dl)  grhob_t,grhor_t,grhoc_t,grhog_t,grhov_t,grhonu_t,gpnu_t,polter
	real(dl) Hchi,shear,aux, pig
	real(dl) k,k2,a,a2
	real(dl) pinu,pir,pnu,adotoa, rhonu
	real(dl) grhoex_t,phi,phidot
	stop 'not fixed for mavans'      
	k2=EV%k2_buf
	k= EV%k_buf   
	aux=EV%aux_buf 


	!E and B start at l=2. Set up pointers accordingly to fill in ayt arrays
	E => ayt(EV%lmaxt+3-1:)
	Eprime=> aytprime(EV%lmaxt+3-1:) 
	B => E(EV%lmaxpolt:)
	Bprime => Eprime(EV%lmaxpolt:)

	cothxor=1._dl/tanfunc(tau/CP%r)/CP%r      

	a=ayt(1)        

	!  Electric part of Weyl tensor and shear tensor
	!        elec=ayt(2)

	Hchi=ayt(2)  

	shear=ayt(3)
	!  Photon anisotropic stress
	pig=ayt(4)

	a2=a*a
	call Quint_ValsAta(a,phi,phidot)
	!  Get sound speed and opacity, and see if should use tight-coupling

	call thermo(tau,cs2,opacity)
	if (k > 0.06_dl*epsw) then
		ep=ep0
	else
		ep=0.2_dl*ep0
	end if
	tcp = ((k/opacity > ep).or.(1._dl/(opacity*tau) > ep)) 

	if (CP%Num_Nu_Massive == 0) then
		rhonu=1._dl
		pnu=1._dl/3._dl
	else
		call Nu_background(a*avarmnu(phi,0),rhonu,pnu) 
	end if
	! Compute expansion rate from: grho=8*pi*rho*a**2
	! Also calculate gpres: 8*pi*p*a**2
	grhob_t=grhob/a
	grhoc_t=grhoc/a
	grhor_t=grhornomass/a2
	grhog_t=grhog/a2
	grhov_t=grhov*a2
	grhormass_t=grhormass/a2

	grhonu_t=grhormass_t*rhonu
	gpnu_t=grhormass_t*pnu

	grho=grhob_t+grhoc_t+grhor_t+grhog_t+grhonu_t

	gpres=(grhog_t+grhor_t)/3._dl +gpnu_t

	if (has_extra) then

		!         call Quint_ValsAta(a,phi,phidot)                        Antz  moved to earlier
		grhoex_t=phidot**2/2 + a2*Vofphi(phi,0)
		grho=grho+grhoex_t
		gpres=gpres-grhoex_t+phidot**2     
	else
		grho=grho +grhov_t
		gpres=gpres - grhov_t
	end if

	adotoa=sqrt((grho+grhok)/3._dl)
	aytprime(1)=adotoa*a
	polter = 0.1_dl*pig + 9._dl/15._dl*E(2)

	if (tcp) then
		!  Don't use tight coupling approx - use explicit equations:
		!  Equation for the photon anisotropic stress
		aytprime(4)=k*(-1._dl/3._dl*EV%Kft(2)*ayt(5)+8._dl/15._dl*shear)  &
			-opacity*(pig - polter)


		do l=3,min(EV%FirstZerolForBeta,EV%lmaxt)-1
		aytprime(l+2)=k*denl(l)*(l*ayt(l+1)-   &
			tensfac(l)*EV%Kft(l)*ayt(l+3))-opacity*ayt(l+2)

		end do

		!Truncate the hierarchy 
		if (EV%lmaxt/=EV%FirstZerolForBeta) then
			aytprime(EV%lmaxt+2)=k*EV%lmaxt/(EV%lmaxt-2._dl)*ayt(EV%lmaxt+1)- &
				(EV%lmaxt+3._dl)*cothxor*ayt(EV%lmaxt+2)-opacity*ayt(EV%lmaxt+2)
		end if

		!E and B-bar equations

		Eprime(2) = - opacity*(E(2) - polter) + k*(4._dl/6._dl*aux*B(2) - &
			5._dl/27._dl*EV%Kft(2)*E(3))

		do l=3,min(EV%FirstZerolForBeta,EV%lmaxpolt)-1
		Eprime(l) =-opacity*E(l) + k*(denl(l)*(l*E(l-1) - &
			tensfacpol(l)*EV%Kft(l)*E(l+1)) + 4._dl/(l*(l+1))*aux*B(l))

		end do

		!truncate: difficult, but zetting to zero seems to work OK
		Eprime(EV%lmaxpolt)=0._dl

		do l=2,min(EV%FirstZerolForBeta,EV%lmaxpolt)-1
		Bprime(l) =-opacity*B(l) + k*(denl(l)*(l*B(l-1) - &
			tensfacpol(l)*EV%Kft(l)*B(l+1)) - 4._dl/(l*(l+1))*aux*E(l))
		end do

		!truncate
		Bprime(EV%lmaxpolt)=0._dl

	else  !Tight coupling
		ayt(4)=32._dl/45._dl*k/opacity*shear  
		E(2)=ayt(4)/4._dl
		aytprime(4:n)=0._dl

	endif


	!  Neutrino equations: 
	!  Anisotropic stress
	if (DoTensorNeutrinos) then

		neutprime => Bprime(EV%lmaxpolt:)
		neut => B(EV%lmaxpolt:)


		!  Massless neutrino anisotropic stress
		pir=neut(2)

		pirdt=k*(-1._dl/3._dl*EV%Kft(2)*neut(3)+ 8._dl/15._dl*shear)
		neutprime(2)=pirdt
		!  And for the moments
		do  l=3,min(EV%FirstZerolForBeta,EV%lmaxnrt)-1
		neutprime(l)=k*denl(l)*(l*neut(l-1)- tensfac(l)*EV%Kft(l)*neut(l+1))
		end do

		!  Truncate the hierarchy
		if (EV%lmaxnrt/=EV%FirstZerolForBeta) then
			neutprime(EV%lmaxnrt)=k*EV%lmaxnrt/(EV%lmaxnrt-2._dl)*neut(EV%lmaxnrt-1)-  &
				(EV%lmaxnrt+3._dl)*cothxor*neut(EV%lmaxnrt)
		endif


		!  Massive neutrino equations of motion.
		if (CP%Num_Nu_massive/= 0) then

			do i=1,nqmax
			q=i*dq-0.5_dl
			aq=a*avarmnu(phi,0)/q
			v=1._dl/sqrt(1._dl+aq*aq)
			akv(i)=k*v
			end do
			do i=1,nqmax
			ind=EV%iqt+i-1                          
			aytprime(ind)=-1._dl/3._dl*akv(i)*EV%Kft(2)*ayt(ind+nqmax)- 2._dl/15._dl*k*shear*dlfdlq(i)
			ind=EV%iqt+i-1+(EV%lmaxnut-2)*nqmax
			!  Truncate moment expansion.
			if (EV%lmaxnut/=EV%FirstZerolForBeta) then
				aytprime(ind)=akv(i)*EV%lmaxnut/(EV%lmaxnut-2._dl)*ayt(ind-nqmax)-(EV%lmaxnut+3)*cothxor*ayt(ind)
			end if         
			end do
			do l=3,min(EV%FirstZerolForBeta,EV%lmaxnut)-1
			do i=1,nqmax
			ind=EV%iqt-1+i+(l-2)*nqmax
			aytprime(ind)=akv(i)*denl(l)*(l*ayt(ind-nqmax)-tensfac(l)*EV%Kft(l)*ayt(ind+nqmax))     
			end do
			end do

			call Nu_Shear(a*avarmnu(phi,0),shearnu,ayt(EV%iqt))                 
			pinu=1.5_dl*shearnu/rhonu   

		else
			pinu=0._dl

		end if


	else
		pinu=0
		pir=0
		pirdt=0  

	end if

	!  Get the propagation equation for the shear
	rhopi=grhog_t*pig+grhor_t*pir+grhonu_t*pinu
	!    aytprime(3)=-adotoa*shear-k*elec-0.5_dl/k*rhopi   

	aytprime(3)=-2*adotoa*shear+k*Hchi*(1+2*CP%curv/k2)-rhopi/k   


	!  And the electric part of the Weyl.
	if (tcp) then
		!  Use the full expression for pigdt
		EV%tenspigdot=aytprime(4)
	else
		!  Use the tight-coupling approximation
		EV%tenspigdot=32._dl/45._dl*k/opacity*(2._dl*adotoa*shear+aytprime(3))
	endif

	aytprime(2)=-k*shear

end subroutine derivst



!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc

end module GaugeInterface
